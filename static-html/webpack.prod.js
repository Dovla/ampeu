const glob = require('glob')
const path = require('path')
const merge = require('webpack-merge')
const PurgeCssPlugin = require('purgecss-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
//const CopyPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const common = require('./webpack.config.js')

const purgeFromTailwind = content => content.match(/[\w-/:]+(?<!:)/g) || []

const htmlPageNames = [
    'index',
    'grid-section',
    'common',
    'header',
    'header-secondary',
    'hero',
    'footer',
    'intro-block',
    'document',
    'news-item',
    'article',
    'special-item',
    'faq',
    'sidebar',
    'tabs-interface',
    'program-2nd-level',
    'agencija-u-brojkama',
    'text-component',
    'video',
    'testimonial',
    'contact',
    'landing-page',
    'pagination',
    'page-settings',
    'filtering',
    'archive'
]

const multipleHtmlPlugins = htmlPageNames.map(name => {
    return new HtmlWebpackPlugin({
        filename: `${name}.html`, // output HTML files
        template: `src/html/${name}.html`, // relative path to the HTML files
        minify: {
            collapseWhitespace: false,
            removeComments: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true,
            useShortDoctype: false
        }
    })
})

module.exports = merge(common, {
    mode: 'production',

    devtool: false,

    plugins: [
        new CleanWebpackPlugin(),

        new PurgeCssPlugin({
            paths: glob.sync(`${path.resolve(__dirname, 'src')}/**/*`, { nodir: true }),
            whitelistPatterns: [/^icon-/,  /^splide__/],
            whitelistPatternsChildren: [/^splide/],
            extractors: [
                {
                    extractor: purgeFromTailwind,
                    extensions: ['html', 'js']
                }
            ]
        }),

        ...multipleHtmlPlugins
    ]
})

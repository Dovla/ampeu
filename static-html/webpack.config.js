const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const WebpackBuildNotifierPlugin = require('webpack-build-notifier')

const env = process.env.NODE_ENV
const path = require('path')

const htmlPageNames = [
    'index',
    'grid-section',
    'common',
    'header',
    'header-secondary',
    'hero',
    'footer',
    'intro-block',
    'document',
    'news-item',
    'article',
    'special-item',
    'faq',
    'sidebar',
    'tabs-interface',
    'program-2nd-level',
    'agencija-u-brojkama',
    'text-component',
    'video',
    'testimonial',
    'contact',
    'landing-page',
    'pagination',
    'page-settings',
    'filtering',
    'archive'
]

const multipleHtmlPlugins = htmlPageNames.map(name => {
    return new HtmlWebpackPlugin({
        filename: `${name}.html`, // output HTML files
        template: `src/html/${name}.html` // relative path to the HTML files
    })
})

module.exports = {
    mode: process.env.NODE_ENV,

    entry: './src/js/main.js',

    output: {
        publicPath: '/',
        filename:
            env === 'development' ? 'js/bundle.js' : 'js/bundle.[contenthash].min.js',
        chunkFilename:
            env === 'development' ? 'js/[name].js' : 'js/[name].[contenthash].min.js',
        path: path.resolve(__dirname, 'dist')
    },

    optimization: {
        splitChunks: {
            chunks: 'all',
            name: 'vendor'
        }
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    env === 'development' ? 'style-loader' : MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: { importLoaders: 2, sourceMap: true }
                    },
                    { loader: 'postcss-loader', options: { sourceMap: true } },
                    {
                        loader: 'sass-loader',
                        options: { implementation: require('sass'), sourceMap: true }
                    }
                ]
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            emitFile: true,
                            name: '[name].[ext]',
                            outputPath: 'images',
                            pubicPath: 'images',
                            esModule: false
                        }
                    }
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            emitFile: true,
                            name: '[name].[ext]',
                            outputPath: 'fonts',
                            pubicPath: 'fonts',
                            esModule: false
                        }
                    }
                ]
            }
        ]
    },

    plugins: [
        new WebpackBuildNotifierPlugin(),

        new MiniCssExtractPlugin({
            filename: 'css/[name].[contenthash].min.css'
        }),

        ...multipleHtmlPlugins
    ]
}

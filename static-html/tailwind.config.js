module.exports = {
    mode: 'jit',
    theme: {
        colors: false,
        screens: {
            'sm': '640px',
            // => @media (min-width: 640px) { ... }

            'md': '768px',
            // => @media (min-width: 768px) { ... }

            'lg': '1024px',
            // => @media (min-width: 1024px) { ... }

            'xl': '1280px'
            // => @media (min-width: 1280px) { ... }
        },
        boxShadow: {
            element: '0 8px 16px rgba(134, 135, 137, 0.15)',
            header: '0 1px 0 rgba(0, 0, 0, 0.05)',
            button: '0 8px 16px rgba(134, 135, 137, 0.1)',
            extra: '0 -4px 4px rgba(0, 0, 0, 0.05)'
        }
    },
    corePlugins: {
        container: false
    },
    plugins: []
}

const fontColor = {
    prop: 'page-settings-font-color',
    variants: {
        default: {
        },
        dark: {
            value: 'dark',
            theme: 'theme--dark',
            data: 'font-color-black'
        },
        light: {
            value: 'light',
            theme: 'theme--light',
            data: 'font-color-light'
        }
    }
}

const fontSize = {
    prop: 'page-settings-font-size',
    data: {
        increment: 'font-size-increment',
        decrement: 'font-size-decrement',
        default: 'font-size-default'
    }
}

const fontFamily = {
    prop: 'page-settings-font-family',
    value: 'dyslexia',
    theme: 'theme--font-dyslexia',
    data: 'font-family-dyslexia'
}

const fontFamilyLexend = {
    prop: 'page-settings-font-family',
    value: 'lexend',
    theme: 'theme--font-lexend',
    data: 'font-family-lexend'
}

export {
    fontColor,
    fontSize,
    fontFamily,
    fontFamilyLexend
}

import '../sass/styles.sass'

// Import page settings helpers
import { fontColor, fontFamily, fontSize, fontFamilyLexend } from './helpers/page-settings'

// Global focusable elements
globalThis.focusableElementsString = `a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]`

/**
 * Imports
 */

// Components
import Testimonial from './components/testimonial'
import AgencyInNumbers from './components/agency-in-numbers'
import LayoutSidebar from './components/layout-sidebar'
import Header from './components/header'
import IntroBlock from './components/intro-block'
import Faq from './components/faq'
import Sidebar from './components/sidebar'
import TabsInterface from './components/tabs-interface'
import PageSettings from './components/page-settings'

/**
 * Map components to selectors
 *
 * @type {*[]}
 */
const components = [
    {
        class: Testimonial,
        selector: '[data-js="cmp-testimonial"]'
    },
    {
        class: AgencyInNumbers,
        selector: '[data-js="cmp-agency-in-numbers"]'
    },
    {
        class: LayoutSidebar,
        selector: '[data-js="cmp-layout-sidebar"]'
    },
    {
        class: Header,
        selector: '[data-js="cmp-header"]'
    },
    {
        class: IntroBlock,
        selector: '[data-js="cmp-intro-block"]'
    },
    {
        class: Faq,
        selector: '[data-js="cmp-faq"]'
    },
    {
        class: Sidebar,
        selector: '[data-js="cmp-sidebar"]'
    },
    {
        class: TabsInterface,
        selector: '[data-js="cmp-tabs"]'
    },
    {
        class: PageSettings,
        selector: '[data-js="cmp-page-settings"]'
    }
]

document.addEventListener('DOMContentLoaded', () => {
    if (window.localStorage) {

        document.documentElement.classList.add('theme')

        // Theme font colors
        if (localStorage.getItem(fontColor.prop) === fontColor?.variants?.light?.value) {
            document.documentElement.classList.add(fontColor?.variants?.light?.theme)
        }

        if (localStorage.getItem(fontColor.prop) === fontColor?.variants?.dark?.value) {
            document.documentElement.classList.add(fontColor?.variants?.dark?.theme)
        }

        // Theme font family
        if (localStorage.getItem(fontFamily.prop) === fontFamily.value) {
            document.documentElement.classList.add(fontFamily.theme)
        }

        if (localStorage.getItem(fontFamilyLexend.prop) === fontFamilyLexend.value) {
            document.documentElement.classList.add(fontFamilyLexend.theme)
        }

        // Theme font size
        if (parseInt(localStorage.getItem(fontSize.prop)) !== 100) {
            document.documentElement.style.setProperty('--font-base', localStorage.getItem(fontSize.prop))
        }
    }
})

/**
 * Apply JS to elements by selectors
 */
components.forEach(component => {
    if (document.querySelector(component.selector) !== null)
        document
            .querySelectorAll(component.selector)
            .forEach(element => new component.class(element, component.options))
})



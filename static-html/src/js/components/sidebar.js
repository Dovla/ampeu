class Sidebar {
    constructor(element) {
        this.rootElement = element

        this.init()
    }

    init() {
        const detailsClass = 'is-collapsed'
        const detailsParentClass = '.Sidebar__item'
        const detailsActionClass = '.Sidebar__action'

        const details = [...this.rootElement.querySelectorAll('[data-js="sidebar-item"]')]

        const handleDetails = e => {
            const button = e.currentTarget

            const target = button.closest(detailsParentClass)
            const actionBtn = button.querySelector(`${detailsActionClass} > svg > use`)
            const targetSiblings = details.filter(detail => detail !== target)

            target.classList.toggle(detailsClass)

            target.setAttribute('aria-expanded', !target.classList.contains(detailsClass) ? 'true' : 'false')

            actionBtn?.setAttribute('xlink:href', target?.classList.contains(detailsClass) ? '#icon-arrow-down' : '#icon-arrow-up')

            targetSiblings.forEach(sibling => {
                sibling.classList.add(detailsClass)
                sibling
                    ?.querySelector(`${detailsActionClass} > svg > use`)
                    ?.setAttribute('xlink:href', '#icon-arrow-down')
                sibling.setAttribute('aria-expanded', 'false')
            })
        }

        details.forEach(detail => {
            const action = detail?.querySelector(detailsActionClass)

            detail.setAttribute('aria-expanded', !detail.classList.contains(detailsClass) ? 'true' : 'false')

            action?.addEventListener('click', e => handleDetails(e))
        })
    }
}

export default Sidebar

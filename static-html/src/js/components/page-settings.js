// Import page settings helpers
import { fontColor, fontFamily, fontSize, fontFamilyLexend } from '../helpers/page-settings'

const largeUpBpQuery = '(min-width: 1024px)'

class PageSettings {
    constructor(element) {
        this.html = document.documentElement
        this.rootElement = element
        this.menu = this.rootElement.querySelector('[data-js="cmp-page-settings-menu"]')
        this.closeMenuTrigger = this.rootElement.querySelector('[data-js="cmp-page-setting-close"]')
        this.triggerToggle = this.rootElement.querySelector('[data-js="cmp-page-setting-toggle"]')
        this.componentClass = 'PageSettings--is-active'
        this.fontColorSettings = this.rootElement.querySelector('[data-js="cmp-settings-font-color"]')
        this.fontSizeSettings = this.rootElement.querySelector('[data-js="cmp-settings-font-size"]')
        this.fontFamilySettings = this.rootElement.querySelector('[data-js="cmp-settings-font-family"]')

        this.store = {
            fontSize: parseInt(localStorage.getItem(fontSize.prop)) || parseInt(getComputedStyle(document.documentElement).getPropertyValue('--font-base'))
        }

        if (window.localStorage) {
            this.init()
        } else {
            this.rootElement.style.display = 'none'

            console.error('[PageSettings.js]: Local storage is not supported.')
        }
    }

    closeMenu() {
        this.rootElement.classList.remove(this.componentClass)
    }

    resizeCloseMenu() {
        const isLargeUp = window.matchMedia(largeUpBpQuery).matches

        if (!isLargeUp && this.rootElement.classList.contains(this.componentClass)) {
            this.rootElement.classList.remove(this.componentClass)
        }
    }

    toggleMenu() {
        this.rootElement.classList.toggle(this.componentClass)
    }

    closeSearchWhenClickedOutside(evt) {
        if (evt === undefined) return

        if (!this.rootElement.contains(evt.target)) {
            this.closeMenu()
        }
    }

    currentOptionData(e) {
        const target = e.target
        const targetData = target?.getAttribute('data-settings')

        return {
            target,
            data: targetData
        }
    }

    siblingElementsClassNameHandler(targetElement, list) {
        const targetList = targetElement.closest('ul') || list
        const listElements = [...targetList.querySelectorAll('[data-settings]')]
        const siblings = listElements.filter(listEl => targetElement !== listEl)

        targetElement.parentElement.classList.add('active')
        siblings.forEach(sibling => sibling.parentElement.classList.remove('active'))
    }

    handleFontColorSettings(e) {
        if (e.type === 'DOMContentLoaded') {

            // Set dark theme localstorage
            if (localStorage.getItem(fontColor.prop) === fontColor?.variants?.dark?.value) {
                this.html.classList.add(fontColor?.variants?.dark?.theme)

                this.siblingElementsClassNameHandler(
                    this.fontColorSettings.querySelector(`[data-settings="${fontColor?.variants?.dark?.data}"]`)
                )
            }

            // Set light theme localstorage
            if (localStorage.getItem(fontColor.prop) === fontColor?.variants?.light?.value) {
                this.html.classList.add(fontColor?.variants?.light?.theme)

                this.siblingElementsClassNameHandler(
                    this.fontColorSettings.querySelector(`[data-settings="${fontColor?.variants?.light?.data}"]`)
                )
            }
        }

        if (e.type === 'click') {
            const { data, target } = this.currentOptionData(e)

            if (data === fontColor.variants.dark.data) {
                this.html.classList.add(fontColor.variants.dark.theme)
                this.html.classList.remove(fontColor?.variants?.light?.theme)
                localStorage.setItem(fontColor.prop, fontColor.variants.dark.value)
            }

            if (data === fontColor.variants.light.data) {
                this.html.classList.add(fontColor.variants.light.theme)
                this.html.classList.remove(fontColor?.variants?.dark?.theme)
                localStorage.setItem(fontColor.prop, fontColor.variants.light.value)
            }

            if (data === 'font-color-default') {
                if (this.html.classList.contains('theme--dark') || this.html.classList.contains('theme--light')) {
                    this.html.classList.remove('theme--dark')
                    this.html.classList.remove('theme--light')
                    localStorage.removeItem(fontColor.prop)
                }
            }

            this.siblingElementsClassNameHandler(target)
        }
    }

    handleFontFamilySettings(e) {

        // Handle DOMContentLoaded event
        if (e.type === 'DOMContentLoaded') {
            if (localStorage.getItem(fontFamily.prop) === fontFamily.value) {
                this.html.classList.add(fontFamily.theme)

                this.siblingElementsClassNameHandler(
                    this.fontFamilySettings.querySelector(`[data-settings="${fontFamily.data}"]`)
                )
            }

            if (localStorage.getItem(fontFamilyLexend.prop) === fontFamilyLexend.value) {
                this.html.classList.add(fontFamilyLexend.theme)

                this.siblingElementsClassNameHandler(
                    this.fontFamilySettings.querySelector(`[data-settings="${fontFamilyLexend.data}"]`)
                )
            }
        }

        // Handle click event
        if (e.type === 'click') {
            const { data, target } = this.currentOptionData(e)

            // Dyslexia font
            if (data === fontFamily.data && !this.html.classList.contains(fontFamily.theme)) {
                this.html.classList.add(fontFamily.theme)
                this.html.classList.remove(fontFamilyLexend.theme)
                localStorage.setItem(fontFamily.prop, fontFamily.value)
            }

            if (data !== fontFamily.data && this.html.classList.contains(fontFamily.theme)) {
                this.html.classList.remove(fontFamily.theme)
                localStorage.removeItem(fontFamily.prop)
            }

            // Lexend font
            if (data === fontFamilyLexend.data && !this.html.classList.contains(fontFamilyLexend.theme)) {
                this.html.classList.add(fontFamilyLexend.theme)
                this.html.classList.remove(fontFamily.theme)
                localStorage.setItem(fontFamilyLexend.prop, fontFamilyLexend.value)
            }

            // Clear the shit
            if (data === 'font-family-default') {
                this.html.classList.remove(fontFamilyLexend.theme)
                this.html.classList.remove(fontFamily.theme)
                localStorage.removeItem(fontFamilyLexend.prop)
                localStorage.removeItem(fontFamily.prop)
            }

            this.siblingElementsClassNameHandler(target)
        }
    }

    textSize(direction) {
        const defaultFontSize = 100
        const step = 10

        let newFontSize

        switch (direction) {
            case 1:
                newFontSize = this.store.fontSize + step
                this.store.fontSize = newFontSize
                break
            case -1:
                newFontSize = this.store.fontSize - step
                this.store.fontSize = newFontSize
                break
            default:
                newFontSize = defaultFontSize
                this.store.fontSize = defaultFontSize
        }

        // Append value to local storage
        localStorage.setItem(fontSize.prop, newFontSize)

        // Set new value in css variable
        this.html.style.setProperty('--font-base', newFontSize)
    }

    handleFontSizeSettings(e) {
        if (e.type === 'DOMContentLoaded') {
            const currentFontSize = parseInt(localStorage.getItem(fontSize.prop))

            if (currentFontSize !== 100) {
                this.html.style.setProperty('--font-base', localStorage.getItem(fontSize.prop))
            }

            if (currentFontSize < 100) {
                this.siblingElementsClassNameHandler(
                    this.fontSizeSettings.querySelector(`[data-settings="${fontSize.data.decrement}"]`)
                )
            } else if (currentFontSize > 100) {
                this.siblingElementsClassNameHandler(
                    this.fontSizeSettings.querySelector(`[data-settings="${fontSize.data.increment}"]`)
                )
            } else {
                this.siblingElementsClassNameHandler(
                    this.fontSizeSettings.querySelector(`[data-settings="${fontSize.data.default}"]`)
                )
            }
        }

        if (e.type === 'click') {
            const { data, target } = this.currentOptionData(e)

            switch (data) {
                case 'font-size-increment':
                    this.textSize(1)
                    break
                case 'font-size-decrement':
                    this.textSize(-1)
                    break
                default:
                    this.textSize(0)
                    break
            }

            this.siblingElementsClassNameHandler(target)
        }
    }

    init() {

        // Add theme class if it doesn't exist
        if (!this.html.classList.contains('theme')) {
            this.html.classList.add('theme')
        }

        this.triggerToggle.addEventListener('click', this.toggleMenu.bind(this))

        // Close menu if window is resized
        window.addEventListener('resize', this.resizeCloseMenu.bind(this), false)

        // Font color settings
        document.addEventListener('DOMContentLoaded', e => this.handleFontColorSettings(e), false)
        this.fontColorSettings?.addEventListener('click', e => this.handleFontColorSettings(e), false)
        //this.fontColorSettings?.addEventListener('keyup', e => this.handleFontColorSettings(e), false)

        // Font size settings
        document.addEventListener('DOMContentLoaded', e => this.handleFontSizeSettings(e), false)
        this.fontSizeSettings.addEventListener('click', e => this.handleFontSizeSettings(e), false)

        // Font dyslexia settings
        document.addEventListener('DOMContentLoaded', e => this.handleFontFamilySettings(e))
        this.fontFamilySettings?.addEventListener('click', e => this.handleFontFamilySettings(e), false)

        // Close menu
        this.closeMenuTrigger.addEventListener('click', this.closeMenu.bind(this))

        // Close modal if is clicked outside the modals content
        document.documentElement.addEventListener('click', e => {
            this.closeSearchWhenClickedOutside(e)
        })

        // Close modal if is clicked esc key
        document.addEventListener('keyup', e => {
            const isLargeUp = window.matchMedia(largeUpBpQuery).matches

            if (e.keyCode === 27 && isLargeUp) {
                this.closeMenu()
            }
        })
    }
}

export default PageSettings

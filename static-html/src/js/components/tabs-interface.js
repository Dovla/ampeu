class TabsInterface {
    constructor(element) {
        this.rootElement = element

        this.init()
    }

    init() {
        const tablist = this.rootElement.querySelector('ul')
        const tabs = [...tablist.querySelectorAll('a')]
        const panels = [...this.rootElement.querySelectorAll('[id^="section"]')]
        const menuTrigger = this.rootElement?.querySelector('button')
        const triggerIcon = menuTrigger.querySelector('use')
        const activeLabel = this.rootElement.querySelector('.Tab.Tab--active-tab')

        const isLargeUp = window.matchMedia('(min-width: 1024px)').matches

        const toggleTablist = () => {
            this.rootElement.classList.toggle('Tabs--is-active')
            const triggerIconValue = triggerIcon.getAttribute('xlink:href')

            triggerIcon.setAttribute('xlink:href', triggerIconValue === '#icon-menu' ? '#icon-close' : '#icon-menu')
        }

        const setActiveTab = () => {
            if (!isLargeUp) {
                const activeTab = tabs.filter(tab => {
                    if (tab.getAttribute('aria-selected') === 'true') {
                        return tab
                    }
                })

                activeLabel.innerHTML = activeTab[0].innerHTML
            }
        }

        menuTrigger.addEventListener('click', toggleTablist)

        // The tab switching function
        const switchTab = (oldTab, newTab) => {
            newTab.focus()
            // Make the active tab focusable by the user (Tab key)
            newTab.removeAttribute('tabindex')
            // Set the selected state
            newTab.setAttribute('aria-selected', 'true')
            oldTab.removeAttribute('aria-selected')
            oldTab.setAttribute('tabindex', '-1')
            // Get the indices of the new and old tabs to find the correct
            // tab panels to show and hide
            let index = Array.prototype.indexOf.call(tabs, newTab)
            let oldIndex = Array.prototype.indexOf.call(tabs, oldTab)
            panels[oldIndex].hidden = true
            panels[index].hidden = false

            setActiveTab()
        }

        // Add the tablist role to the first <ul> in the .tabbed container
        tablist.setAttribute('role', 'tablist')

        // Add semantics are remove user focusability for each tab
        tabs.forEach((tab, i) => {
            tab.setAttribute('role', 'tab')
            tab.setAttribute('id', 'tab-' + (i + 1))
            tab.setAttribute('tabindex', '-1')
            tab.parentNode.setAttribute('role', 'presentation')

            // Handle clicking of tabs for mouse users
            tab.addEventListener('click', e => {
                e.preventDefault()

                toggleTablist()

                let currentTab = tablist.querySelector('[aria-selected]')
                if (e.currentTarget !== currentTab) {
                    switchTab(currentTab, e.currentTarget)
                }
            })

            // Handle keydown events for keyboard users
            tab.addEventListener('keydown', e => {
                // Get the index of the current tab in the tabs node list
                let index = Array.prototype.indexOf.call(tabs, e.currentTarget)
                // Work out which key the user is pressing and
                // Calculate the new tab's index where appropriate
                let dir = e.keyCode === 38 ? index - 1 : e.keyCode === 40 || e.keyCode === 9 ? index + 1 : e.keyCode === 39 ? 'right' : null

                if (dir !== undefined) {
                    e.preventDefault()
                    // If the right key is pressed, move focus to the open panel,
                    // otherwise switch to the adjacent tab
                    dir === 'right' ? panels[i].focus() : tabs[dir] ? switchTab(e.currentTarget, tabs[dir]) : void 0
                }
            })
        })

        // Add tab panel semantics and hide them all
        panels.forEach((panel, i) => {
            panel.setAttribute('role', 'tabpanel')
            panel.setAttribute('tabindex', '-1')
            //let id = panel.getAttribute('id')
            panel.setAttribute('aria-labelledby', tabs[i].id)
            panel.hidden = true
        })

        // Initially activate the first tab and reveal the first tab panel
        tabs[0].removeAttribute('tabindex')
        tabs[0].setAttribute('aria-selected', 'true')
        panels[0].hidden = false

        document.addEventListener('DOMContentLoaded', setActiveTab)
        window.addEventListener('resize', setActiveTab)
    }
}

export default TabsInterface

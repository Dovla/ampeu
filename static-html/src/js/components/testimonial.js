import Splide from '@splidejs/splide'
import '@splidejs/splide/dist/css/splide-core.min.css'

class Testimonial {
    constructor(element) {
        this.rootElement = element

        this.init()
    }

    init() {
        document.addEventListener('DOMContentLoaded', () => {
            new Splide(this.rootElement, {
                arrows: false,
                gap: 0,
                type: 'fade',
                perPage: 1
            }).mount()
        })
    }
}

export default Testimonial


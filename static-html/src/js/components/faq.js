class Faq {
    constructor(element) {
        this.rootElement = element

        this.init()
    }

    init() {
        const detailsClass = 'is-collapsed'
        const detailsActionClass = '.Faq__action'
        const detailsHeaderClass = '.Faq__header'

        const details = [...this.rootElement.querySelectorAll('[data-js="faq"]')]

        const handleDetails = (e, keyboard = false) => {
            const target = keyboard ? e.currentTarget : e.currentTarget.parentElement
            const actionBtn = target?.querySelector(`${detailsActionClass} > svg > use`)

            target.classList.toggle(detailsClass)

            target.setAttribute('aria-expanded', !target.classList.contains(detailsClass) ? 'true' : 'false')

            actionBtn?.setAttribute('xlink:href', target?.classList.contains(detailsClass) ? '#icon-plus' : '#icon-minus')

            const targetSiblings = details.filter(detail => detail !== target)

            targetSiblings.forEach(sibling => {
                sibling.classList.add(detailsClass)
                sibling
                    ?.querySelector(`${detailsActionClass} > svg > use`)
                    ?.setAttribute('xlink:href', '#icon-plus')
                sibling.setAttribute('aria-expanded', 'false')
            })
        }

        details.forEach(detail => {
            const action = detail.querySelector(detailsHeaderClass)
            const firstDetail = details?.[0]
            const firstDetailActionSymbol = firstDetail?.querySelector(`${detailsActionClass} > svg > use`)

            firstDetail.classList.remove(detailsClass)

            detail.setAttribute('aria-expanded', !detail.classList.contains(detailsClass) ? 'true' : 'false')

            firstDetailActionSymbol?.setAttribute('xlink:href', '#icon-minus')

            action.addEventListener('click', e => handleDetails(e))

            detail.addEventListener('keydown', e => {
                if (e.keyCode === 13) {
                    handleDetails(e, true)
                }
            })
        })
    }
}

export default Faq

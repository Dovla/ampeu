class IntroBlock {
    constructor(element) {
        this.rootElement = element

        this.init()
    }

    init() {
        const orderContentHandler = () => {
            const isMediumUp = window.matchMedia('(min-width: 768px)').matches

            const containerElement = this.rootElement?.getElementsByClassName('IntroBlock__container')[0]
            const imageElement = this.rootElement?.getElementsByClassName('IntroBlock__image')[0]
            const headerElement = this.rootElement?.getElementsByClassName('IntroBlock__header')[0]

            const containerChildNodesNumber = containerElement.childElementCount

            if (!imageElement) {
                return null
            }

            if (isMediumUp) {
                containerElement.append(imageElement)
            } else if (containerChildNodesNumber > 1) {
                headerElement.after(imageElement)
            }
        }

        window.addEventListener('DOMContentLoaded', orderContentHandler)
        window.addEventListener('resize', orderContentHandler)
    }
}

export default IntroBlock

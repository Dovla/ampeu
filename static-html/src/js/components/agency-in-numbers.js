class AgencyInNumbers {
    constructor(element) {
        this.rootElement = element
    }

    init() {
        const documentRoot = document.documentElement
        const header = this.rootElement.querySelector('header')
        const svgContainer = this.rootElement.querySelector('section')
        const events = ['DOMContentLoaded', 'resize']

        const svgHeightFn = () => {
            return svgContainer.clientHeight
        }

        const getHeaderHeight = () => {
            return header.clientHeight
        }

        const getContainerPadding = () => {
            return window.getComputedStyle(this.rootElement, null).getPropertyValue('padding-top')
        }

        const setContainerMargin = () => {
            const headerHeight = getHeaderHeight()
            const containerPadding = getContainerPadding()
            const height = headerHeight + (Number(containerPadding.split('px')[0]) / 2)

            if (window.matchMedia('(min-width: 1024px)').matches) {
                documentRoot.style.setProperty('--ain-position', `-${height}px`)
            } else {
                documentRoot.style.setProperty('--ain-position', `-11vw`)
            }
        }

        events.forEach(event => {
            window.addEventListener(event, () => {
                svgHeightFn()
                setContainerMargin()
            })
        })
    }
}

export default AgencyInNumbers



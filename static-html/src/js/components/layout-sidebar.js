class LayoutSidebar {
    constructor(element) {
        this.rootElement = element

        this.init()
    }

    init() {
        const asideElement = this.rootElement.querySelector('aside')
        const sectionElement = this.rootElement.querySelector('section')
        const sidebarOptionsShowMobile = this.rootElement.getAttribute('data-option-show-mobile') !== null

        const headerHeightFn = () => {
            const isLargeUp = window.matchMedia('(min-width: 1024px)').matches

            const sectionHeader = sectionElement.querySelector('header')
            const sectionHeaderHeight = sectionHeader.offsetHeight
            const headerStyle = getComputedStyle(sectionHeader)
            const headerMarginBottom = parseInt(headerStyle.marginBottom)

            asideElement.setAttribute('aria-expanded', isLargeUp ? 'true' : 'false')

            console.log('sidebarOptions', sidebarOptionsShowMobile)


            if (!isLargeUp) {
                asideElement.removeAttribute('style')

                if (sidebarOptionsShowMobile) return;
                asideElement.setAttribute('hidden', '')
            } else {
                asideElement.style.paddingTop = `${sectionHeaderHeight + headerMarginBottom}px`

                if (sidebarOptionsShowMobile) return;
                asideElement.removeAttribute('hidden')
            }
        }

        document.addEventListener('DOMContentLoaded', headerHeightFn)
        window.addEventListener('resize', headerHeightFn)
    }
}

export default LayoutSidebar

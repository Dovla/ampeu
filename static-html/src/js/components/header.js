// @TODO: unify isLargeUp variable

const largeUpBpQuery = '(min-width: 1024px)'

class Header {
    constructor(element) {
        this.rootElement = element
        this.documentRoot = document.documentElement
        this.bodyElement = this.documentRoot?.querySelector('body')
        this.className = {
            dropdownOpened: 'active-dropdown',
            menuOpened: 'menu--is-opened',
            preventScroll: 'prevent-scroll'
        }
        this.buttonContent = `<svg class="icon-container icon-1.5x"><use xlink:href="#icon-arrow-down" /></svg`

        this.init()
    }

    mobileNavigationInit() {
        if (document.querySelector('[data-js="cmp-mobile-navigation"]')) {
            const mobileNavigation = document.querySelector('[data-js="cmp-mobile-navigation"]')
            const handleMobileDropdownNavigation = () => {
                const isLargeUp = window.matchMedia(largeUpBpQuery).matches
                const mobileListWithDropdown = [...mobileNavigation?.querySelectorAll('.has-mobile-dropdown')]

                mobileListWithDropdown.forEach(dropdown => {
                    const link = dropdown.querySelector('a')
                    const dropdownList = dropdown.querySelector('ul')

                    const fragmentButton = document.createDocumentFragment()
                    const mobileListWrapper = document.createElement('div')
                    const mobileDropdownBtnTrigger = document.createElement('button')
                    const firstElementIsAnchor = dropdown.firstElementChild.tagName.toLowerCase() === 'a'

                    if (!isLargeUp && firstElementIsAnchor) {
                        mobileDropdownBtnTrigger.innerHTML = this.buttonContent
                        fragmentButton.appendChild(mobileDropdownBtnTrigger)
                        dropdown.appendChild(fragmentButton)

                        mobileListWrapper.appendChild(link)
                        mobileListWrapper.appendChild(mobileDropdownBtnTrigger)

                        dropdown.insertBefore(mobileListWrapper, dropdown.firstChild)
                    }

                    if (!isLargeUp) {
                        mobileDropdownBtnTrigger?.addEventListener('click', e => {
                            const target = e.currentTarget
                            const targetLi = target.parentElement.parentElement
                            const filterSiblings = mobileListWithDropdown?.filter(list => list !== targetLi)

                            if (filterSiblings) {
                                filterSiblings.forEach(filterSibling => {
                                    if (filterSibling.classList.contains(this.className.dropdownOpened)) {
                                        filterSibling.classList.remove(this.className.dropdownOpened)
                                    }
                                })
                            }

                            dropdownList.parentElement.classList.toggle(this.className.dropdownOpened)
                        })
                    }

                    if (isLargeUp) {
                        if (dropdown.classList.contains('has-mobile-dropdown')) {
                            dropdown.classList.remove('has-mobile-dropdown')
                        }

                        if (!!dropdownList) {
                            dropdownList.remove()
                        }
                    }
                })
            }

            document.addEventListener('DOMContentLoaded', handleMobileDropdownNavigation)
            window.addEventListener('resize', handleMobileDropdownNavigation)
        }
    }

    desktopNavigationInit() {
        const triggerDesktopMenu = this.rootElement?.querySelector('[data-js="cmp-desktop-menu-trigger"]')
        const triggerDesktopMenuSvg = triggerDesktopMenu?.querySelector('use')

        const blockFocusableElements = [...this.rootElement.querySelectorAll(globalThis.focusableElementsString)]
        const lastFocusableElement = blockFocusableElements[blockFocusableElements.length - 1]

        const handleMenuAction = action => {
            const actionTrigger = action === 'close' ? 'remove' : 'add'
            this.bodyElement.classList[actionTrigger](this.className.menuOpened)
            this.bodyElement.classList[actionTrigger](this.className.preventScroll)
            triggerDesktopMenuSvg.setAttribute('xlink:href', `#icon-${action === 'close' ? 'menu' : 'close'}`)
        }

        const handleDesktopMenu = () => {
            const isLargeUp = window.matchMedia(largeUpBpQuery).matches

            if (!isLargeUp) {
                if (this.bodyElement.classList.contains(this.className.menuOpened)) {
                    this.bodyElement.classList.remove(this.className.menuOpened)
                }
                return
            }

            this.bodyElement.classList.contains(this.className.menuOpened) ? handleMenuAction('close') : handleMenuAction('open')

        }

        const closeDesktopMenuOnResize = () => {
            const isLargeUp = window.matchMedia(largeUpBpQuery).matches

            if (!isLargeUp && this.bodyElement.classList.contains(this.className.menuOpened)) {
                handleMenuAction('close')
            }
        }

        triggerDesktopMenu?.addEventListener('click', handleDesktopMenu)
        lastFocusableElement.addEventListener('focusout', () => triggerDesktopMenu.focus())
        window.addEventListener('resize', closeDesktopMenuOnResize)
        document.addEventListener('DOMContentLoaded', closeDesktopMenuOnResize)
        document.addEventListener('keyup', e => {
            const isLargeUp = window.matchMedia(largeUpBpQuery).matches

            if (e.keyCode === 27 && isLargeUp) {
                handleMenuAction('close')
            }
        })
    }

    init() {
        const extraBlock = this.rootElement?.querySelector('[data-js="cmp-extra"]')
        const trigger = this.rootElement?.querySelector('[data-js="cmp-menu-trigger"]')
        const triggerSvgElement = trigger?.querySelector('svg > use')
        const headerActiveClass = 'Header--isOpen'
        const headerFixedClass = 'Header--isFixed'

        const menuPositionName = '--menu-position'
        const menuHeightName = '--menu-height'
        const headerHeightInnerName = '--header-inner-height'

        const handleHeaderHeight = () => {
            const headerHeight = this.rootElement.querySelector('[data-js="main-navigation-mobile"]').offsetHeight
            const extraBlockHeight = extraBlock.offsetHeight
            const windowHeight = window.innerHeight || document.documentElement.clientHeight ||
                document.body.clientHeight

            const menuHeight = windowHeight - headerHeight - extraBlockHeight

            if (this.rootElement.classList.contains(headerFixedClass)) {
                this.documentRoot.style.setProperty(menuPositionName, `${headerHeight / 16}rem`)
            }
            this.documentRoot.style.setProperty(menuHeightName, `${menuHeight / 16}rem`)
            this.documentRoot.style.setProperty(headerHeightInnerName, `${headerHeight / 16}rem`)
        }
        const handleTriggerIcon = () => {
            if (this.rootElement.classList.contains(headerActiveClass)) {
                this.rootElement.classList.remove(headerActiveClass)
                trigger.setAttribute('aria-expanded', 'false')
            } else {
                this.rootElement.classList.add(headerActiveClass)
                trigger.setAttribute('aria-expanded', 'true')
            }

            if (this.rootElement.classList.contains(headerActiveClass)) {
                this.bodyElement.classList.add('prevent-scroll')
                triggerSvgElement.setAttribute('xlink:href', '#icon-close')
            } else {
                this.bodyElement.classList.remove('prevent-scroll')
                triggerSvgElement.setAttribute('xlink:href', '#icon-menu')
            }
        }

        document.addEventListener('DOMContentLoaded', () => {
            setTimeout(() => {
                handleTriggerIcon()
                handleHeaderHeight()
            })
        })

        trigger.addEventListener('click', handleTriggerIcon)
        window.addEventListener('resize', handleHeaderHeight)

        window.addEventListener('resize', () => {
            const isLargeUp = window.matchMedia(largeUpBpQuery).matches

            if (isLargeUp) {
                this.rootElement.classList.remove(headerActiveClass)
                triggerSvgElement.setAttribute('xlink:href', '#icon-menu')

                if (!this.bodyElement.classList.contains('menu--is-opened')) {
                    this.bodyElement.classList.remove('prevent-scroll')
                }
            }
        })

        this.mobileNavigationInit()
        this.desktopNavigationInit()
    }
}

export default Header

<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer/src'),
    'yii\\queue\\sync\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/sync'),
    'yii\\queue\\stomp\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/stomp'),
    'yii\\queue\\sqs\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/sqs'),
    'yii\\queue\\redis\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/redis'),
    'yii\\queue\\gearman\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/gearman'),
    'yii\\queue\\file\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/file'),
    'yii\\queue\\db\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/db'),
    'yii\\queue\\beanstalk\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/beanstalk'),
    'yii\\queue\\amqp_interop\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/amqp_interop'),
    'yii\\queue\\amqp\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/amqp'),
    'yii\\queue\\' => array($vendorDir . '/yiisoft/yii2-queue/src'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug/src'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'yii2tech\\ar\\softdelete\\' => array($vendorDir . '/yii2tech/ar-softdelete/src'),
    'weareferal\\remotesync\\' => array($vendorDir . '/weareferal/remote-sync/src'),
    'weareferal\\remotecore\\' => array($vendorDir . '/weareferal/remote-core/src'),
    'voku\\helper\\' => array($vendorDir . '/voku/email-check/src/voku/helper', $vendorDir . '/voku/urlify/src/voku/helper', $vendorDir . '/voku/anti-xss/src/voku/helper'),
    'voku\\' => array($vendorDir . '/voku/portable-ascii/src/voku', $vendorDir . '/voku/portable-utf8/src/voku', $vendorDir . '/voku/stop-words/src/voku'),
    'verbb\\supertable\\' => array($vendorDir . '/verbb/super-table/src'),
    'verbb\\fieldmanager\\' => array($vendorDir . '/verbb/field-manager/src'),
    'verbb\\base\\' => array($vendorDir . '/verbb/base/src'),
    'topshelfcraft\\wordsmith\\' => array($vendorDir . '/topshelfcraft/wordsmith/src'),
    'supercool\\buttonbox\\' => array($vendorDir . '/supercool/buttonbox/src'),
    'spicyweb\\embeddedassets\\' => array($vendorDir . '/spicyweb/craft-embedded-assets/src'),
    'spacecatninja\\imagerx\\' => array($vendorDir . '/spacecatninja/imager-x/src'),
    'putyourlightson\\sprig\\plugin\\' => array($vendorDir . '/putyourlightson/craft-sprig/src'),
    'putyourlightson\\sprig\\' => array($vendorDir . '/putyourlightson/craft-sprig-core/src'),
    'putyourlightson\\sendgrid\\' => array($vendorDir . '/putyourlightson/craft-sendgrid/src'),
    'phpseclib3\\' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/type-resolver/src', $vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'nystudio107\\seomatic\\' => array($vendorDir . '/nystudio107/craft-seomatic/src'),
    'nystudio107\\pluginmanifest\\' => array($vendorDir . '/nystudio107/craft-plugin-manifest/src'),
    'modules\\' => array($baseDir . '/modules'),
    'mmikkel\\reasons\\' => array($vendorDir . '/mmikkel/reasons/src'),
    'mikehaertl\\shellcommand\\' => array($vendorDir . '/mikehaertl/php-shellcommand/src'),
    'kornrunner\\Blurhash\\' => array($vendorDir . '/kornrunner/blurhash/src'),
    'fruitstudios\\linkit\\' => array($vendorDir . '/fruitstudios/linkit/src'),
    'ether\\simplemap\\' => array($vendorDir . '/ether/simplemap/src'),
    'enshrined\\svgSanitize\\' => array($vendorDir . '/enshrined/svg-sanitize/src'),
    'elleracompany\\cookieconsent\\' => array($vendorDir . '/elleracompany/craft-cookie-consent/src'),
    'dukt\\analytics\\' => array($vendorDir . '/dukt/analytics/src'),
    'creocoder\\nestedsets\\' => array($vendorDir . '/creocoder/yii2-nested-sets/src'),
    'crafttests\\fixtures\\' => array($vendorDir . '/craftcms/cms/tests/fixtures'),
    'craftcms\\oauth2\\client\\' => array($vendorDir . '/craftcms/oauth2-craftid/src'),
    'craft\\redactor\\' => array($vendorDir . '/craftcms/redactor/src'),
    'craft\\composer\\' => array($vendorDir . '/craftcms/plugin-installer/src'),
    'craft\\' => array($vendorDir . '/craftcms/cms/src'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'benf\\neo\\' => array($vendorDir . '/spicyweb/craft-neo/src'),
    'aelvan\\mailchimpsubscribe\\' => array($vendorDir . '/aelvan/mailchimp-subscribe/src'),
    'What3words\\Geocoder\\' => array($vendorDir . '/what3words/w3w-php-wrapper/src'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'TrueBV\\' => array($vendorDir . '/true/punycode/src'),
    'Tinify\\' => array($vendorDir . '/tinify/tinify/lib/Tinify'),
    'TheIconic\\Tracking\\GoogleAnalytics\\' => array($vendorDir . '/theiconic/php-ga-measurement-protocol/src'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Php73\\' => array($vendorDir . '/symfony/polyfill-php73'),
    'Symfony\\Polyfill\\Php72\\' => array($vendorDir . '/symfony/polyfill-php72'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Normalizer\\' => array($vendorDir . '/symfony/polyfill-intl-normalizer'),
    'Symfony\\Polyfill\\Intl\\Idn\\' => array($vendorDir . '/symfony/polyfill-intl-idn'),
    'Symfony\\Polyfill\\Intl\\Grapheme\\' => array($vendorDir . '/symfony/polyfill-intl-grapheme'),
    'Symfony\\Polyfill\\Iconv\\' => array($vendorDir . '/symfony/polyfill-iconv'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Contracts\\Translation\\' => array($vendorDir . '/symfony/translation-contracts'),
    'Symfony\\Contracts\\Service\\' => array($vendorDir . '/symfony/service-contracts'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\String\\' => array($vendorDir . '/symfony/string'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Stringy\\' => array($vendorDir . '/voku/stringy/src'),
    'SendGrid\\Stats\\' => array($vendorDir . '/sendgrid/sendgrid/lib/stats'),
    'SendGrid\\Mail\\' => array($vendorDir . '/sendgrid/sendgrid/lib/mail'),
    'SendGrid\\Helper\\' => array($vendorDir . '/sendgrid/sendgrid/lib/helper'),
    'SendGrid\\EventWebhook\\' => array($vendorDir . '/sendgrid/sendgrid/lib/eventwebhook'),
    'SendGrid\\Contacts\\' => array($vendorDir . '/sendgrid/sendgrid/lib/contacts'),
    'SendGrid\\' => array($vendorDir . '/sendgrid/php-http-client/lib'),
    'Seld\\PharUtils\\' => array($vendorDir . '/seld/phar-utils/src'),
    'Seld\\JsonLint\\' => array($vendorDir . '/seld/jsonlint/src/Seld/JsonLint'),
    'Seld\\CliPrompt\\' => array($vendorDir . '/seld/cli-prompt/src'),
    'SSNepenthe\\ColorUtils\\' => array($vendorDir . '/ssnepenthe/color-utils/src'),
    'Rize\\' => array($vendorDir . '/rize/uri-template/src/Rize'),
    'React\\Promise\\' => array($vendorDir . '/react/promise/src'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'PhpScience\\TextRank\\' => array($vendorDir . '/php-science/textrank/src', $vendorDir . '/php-science/textrank/tests'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src/PhpOption'),
    'ParagonIE\\ConstantTime\\' => array($vendorDir . '/paragonie/constant_time_encoding/src'),
    'Opis\\Closure\\' => array($vendorDir . '/opis/closure/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'MaxMind\\WebService\\' => array($vendorDir . '/maxmind/web-service-common/src/WebService'),
    'MaxMind\\Exception\\' => array($vendorDir . '/maxmind/web-service-common/src/Exception'),
    'MaxMind\\Db\\' => array($vendorDir . '/maxmind-db/reader/src/MaxMind/Db'),
    'Masterminds\\' => array($vendorDir . '/masterminds/html5/src'),
    'Mapkit\\' => array($vendorDir . '/mapkit/jwt/src'),
    'Mailchimp\\' => array($vendorDir . '/pacely/mailchimp-apiv3/src'),
    'LitEmoji\\' => array($vendorDir . '/elvanto/litemoji/src'),
    'League\\OAuth2\\Client\\' => array($vendorDir . '/league/oauth2-client/src', $vendorDir . '/league/oauth2-google/src'),
    'League\\MimeTypeDetection\\' => array($vendorDir . '/league/mime-type-detection/src'),
    'League\\Flysystem\\' => array($vendorDir . '/league/flysystem/src'),
    'Laminas\\ZendFrameworkBridge\\' => array($vendorDir . '/laminas/laminas-zendframework-bridge/src'),
    'Laminas\\Stdlib\\' => array($vendorDir . '/laminas/laminas-stdlib/src'),
    'Laminas\\Feed\\' => array($vendorDir . '/laminas/laminas-feed/src'),
    'Laminas\\Escaper\\' => array($vendorDir . '/laminas/laminas-escaper/src'),
    'JsonSchema\\' => array($vendorDir . '/justinrainbow/json-schema/src/JsonSchema'),
    'JmesPath\\' => array($vendorDir . '/mtdowling/jmespath.php/src'),
    'Imagine\\' => array($vendorDir . '/pixelandtonic/imagine/src'),
    'Illuminate\\Support\\' => array($vendorDir . '/illuminate/support'),
    'Illuminate\\Contracts\\' => array($vendorDir . '/illuminate/contracts'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'GraphQL\\' => array($vendorDir . '/webonyx/graphql-php/src'),
    'Google\\Service\\' => array($vendorDir . '/google/apiclient-services/src'),
    'Google\\Cloud\\Storage\\' => array($vendorDir . '/google/cloud-storage/src'),
    'Google\\Cloud\\Core\\' => array($vendorDir . '/google/cloud-core/src'),
    'Google\\CRC32\\' => array($vendorDir . '/google/crc32/src'),
    'Google\\Auth\\' => array($vendorDir . '/google/auth/src'),
    'Google\\' => array($vendorDir . '/google/apiclient/src'),
    'GeoIp2\\' => array($vendorDir . '/geoip2/geoip2/src'),
    'Firebase\\JWT\\' => array($vendorDir . '/firebase/php-jwt/src'),
    'Embed\\' => array($vendorDir . '/embed/embed/src'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/src'),
    'Dotenv\\' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Doctrine\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Inflector'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib/Doctrine/Common/Lexer'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Common/Inflector'),
    'Defuse\\Crypto\\' => array($vendorDir . '/defuse/php-encryption/src'),
    'Composer\\XdebugHandler\\' => array($vendorDir . '/composer/xdebug-handler/src'),
    'Composer\\Spdx\\' => array($vendorDir . '/composer/spdx-licenses/src'),
    'Composer\\Semver\\' => array($vendorDir . '/composer/semver/src'),
    'Composer\\MetadataMinifier\\' => array($vendorDir . '/composer/metadata-minifier/src'),
    'Composer\\CaBundle\\' => array($vendorDir . '/composer/ca-bundle/src'),
    'Composer\\' => array($vendorDir . '/composer/composer/src/Composer'),
    'ColorThief\\' => array($vendorDir . '/ksubileau/color-thief-php/lib/ColorThief'),
    'Carbon\\' => array($vendorDir . '/nesbot/carbon/src/Carbon'),
    'Aws\\' => array($vendorDir . '/aws/aws-sdk-php/src'),
    'Arrayy\\' => array($vendorDir . '/voku/arrayy/src'),
);

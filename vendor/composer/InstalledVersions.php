<?php

/*
 * This file is part of Composer.
 *
 * (c) Nils Adermann <naderman@naderman.de>
 *     Jordi Boggiano <j.boggiano@seld.be>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Composer;

use Composer\Autoload\ClassLoader;
use Composer\Semver\VersionParser;

/**
 * This class is copied in every Composer installed project and available to all
 *
 * See also https://getcomposer.org/doc/07-runtime.md#installed-versions
 *
 * To require it's presence, you can require `composer-runtime-api ^2.0`
 */
class InstalledVersions
{
    private static $installed = array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '34b03842975be4770846e2f95d2f23214c98289c',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '34b03842975be4770846e2f95d2f23214c98289c',
    ),
    'aelvan/mailchimp-subscribe' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '83029cb054fc00d705b823d5fef8d02fe803e250',
    ),
    'aws/aws-sdk-php' => 
    array (
      'pretty_version' => '3.191.1',
      'version' => '3.191.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '221eb3790a2f5cc73067ec1c13d4c788a0296351',
    ),
    'bower-asset/inputmask' => 
    array (
      'provided' => 
      array (
        0 => '~3.2.2 | ~3.3.5',
      ),
    ),
    'bower-asset/jquery' => 
    array (
      'provided' => 
      array (
        0 => '3.5.*@stable | 3.4.*@stable | 3.3.*@stable | 3.2.*@stable | 3.1.*@stable | 2.2.*@stable | 2.1.*@stable | 1.11.*@stable | 1.12.*@stable',
      ),
    ),
    'bower-asset/punycode' => 
    array (
      'provided' => 
      array (
        0 => '1.3.*',
      ),
    ),
    'bower-asset/yii2-pjax' => 
    array (
      'provided' => 
      array (
        0 => '~2.0.1',
      ),
    ),
    'cebe/markdown' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9bac5e971dd391e2802dca5400bbeacbaea9eb86',
    ),
    'composer/ca-bundle' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4c679186f2aca4ab6a0f1b0b9cf9252decb44d0b',
    ),
    'composer/composer' => 
    array (
      'pretty_version' => '2.0.13',
      'version' => '2.0.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '986e8b86b7b570632ad0a905c3726c33dd4c0efb',
    ),
    'composer/metadata-minifier' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c549d23829536f0d0e984aaabbf02af91f443207',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '3.2.6',
      'version' => '3.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '83e511e247de329283478496f7a1e114c9517506',
    ),
    'composer/spdx-licenses' => 
    array (
      'pretty_version' => '1.5.5',
      'version' => '1.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'de30328a7af8680efdc03e396aad24befd513200',
    ),
    'composer/xdebug-handler' => 
    array (
      'pretty_version' => '1.4.6',
      'version' => '1.4.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f27e06cd9675801df441b3656569b328e04aa37c',
    ),
    'craftcms/cms' => 
    array (
      'pretty_version' => '3.7.11',
      'version' => '3.7.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '97447d4bedc9151052ec0d080672905584b0b6e1',
    ),
    'craftcms/oauth2-craftid' => 
    array (
      'pretty_version' => '1.0.0.1',
      'version' => '1.0.0.1',
      'aliases' => 
      array (
      ),
      'reference' => '3f18364139d72d83fb50546d85130beaaa868836',
    ),
    'craftcms/plugin-installer' => 
    array (
      'pretty_version' => '1.5.7',
      'version' => '1.5.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '23ec472acd4410b70b07d5a02b2b82db9ee3f66b',
    ),
    'craftcms/redactor' => 
    array (
      'pretty_version' => '2.8.8',
      'version' => '2.8.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c0272678a9d9bdc3990b36079e42667acdcd7016',
    ),
    'craftcms/server-check' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d8345bc7920b6657bd3fac396efee6bf8609ed6',
    ),
    'creocoder/yii2-nested-sets' => 
    array (
      'pretty_version' => '0.9.0',
      'version' => '0.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb8635a459b6246e5a144f096b992dcc30cf9954',
    ),
    'danielstjules/stringy' => 
    array (
      'replaced' => 
      array (
        0 => '~3.0',
      ),
    ),
    'davechild/textstatistics' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd16252a5c78fad6f8ea4fcd7e55d14fd07b1382',
    ),
    'defuse/php-encryption' => 
    array (
      'pretty_version' => 'v2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '77880488b9954b7884c25555c2a0ea9e7053f9d2',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4650c8b30c753a76bf44fb2ed00117d6f367490c',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'dukt/analytics' => 
    array (
      'pretty_version' => '4.0.13',
      'version' => '4.0.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '60b50a424dd7f7fb3def92a0b7a60f12a29b26a4',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee0db30118f661fb166bcffbf5d82032df484697',
    ),
    'elleracompany/craft-cookie-consent' => 
    array (
      'pretty_version' => '1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '89d9136baabb4babcb05f92842f0abea6a7d7736',
    ),
    'elvanto/litemoji' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acd6fd944814683983dcdfcf4d33f24430631b77',
    ),
    'embed/embed' => 
    array (
      'pretty_version' => '3.4.17',
      'version' => '3.4.17.0',
      'aliases' => 
      array (
      ),
      'reference' => '2c0e112f7332597ec6a55174f2353e04859ba356',
    ),
    'enshrined/svg-sanitize' => 
    array (
      'pretty_version' => '0.14.1',
      'version' => '0.14.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '307b42066fb0b76b5119f5e1f0826e18fefabe95',
    ),
    'erusev/parsedown' => 
    array (
      'pretty_version' => '1.7.4',
      'version' => '1.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb17b6477dfff935958ba01325f2e8a2bfa6dab3',
    ),
    'erusev/parsedown-extra' => 
    array (
      'pretty_version' => '0.8.1',
      'version' => '0.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '91ac3ff98f0cea243bdccc688df43810f044dcef',
    ),
    'ether/simplemap' => 
    array (
      'pretty_version' => '3.9.2',
      'version' => '3.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ad6e01566737eff0b7d536e51ac1bfbf9cc24f6',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v5.4.0',
      'version' => '5.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2113d9b2e0e349796e72d2a63cf9319100382d2',
    ),
    'fruitstudios/linkit' => 
    array (
      'pretty_version' => '1.1.12.1',
      'version' => '1.1.12.1',
      'aliases' => 
      array (
      ),
      'reference' => 'c5d9e6d6e05292dd14f80196ba8d9348b14df3f0',
    ),
    'geoip2/geoip2' => 
    array (
      'pretty_version' => 'v2.11.0',
      'version' => '2.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd01be5894a5c1a3381c58c9b1795cd07f96c30f7',
    ),
    'google/apiclient' => 
    array (
      'pretty_version' => 'v2.10.1',
      'version' => '2.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '11871e94006ce7a419bb6124d51b6f9ace3f679b',
    ),
    'google/apiclient-services' => 
    array (
      'pretty_version' => 'v0.208.0',
      'version' => '0.208.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '77fdacfc99f582970dcb3c41233158b02cc4ba5e',
    ),
    'google/auth' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4da3a850e9d1045918099b6561165d829efbe2b7',
    ),
    'google/cloud-core' => 
    array (
      'pretty_version' => 'v1.42.2',
      'version' => '1.42.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3fff3ca4af92c87eb824e5c98aaf003523204a2',
    ),
    'google/cloud-storage' => 
    array (
      'pretty_version' => 'v1.24.1',
      'version' => '1.24.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '440e195a11dbb9a6a98818dc78ba09857fbf7ebd',
    ),
    'google/crc32' => 
    array (
      'pretty_version' => 'v0.1.0',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8525f0dea6fca1893e1bae2f6e804c5f7d007fb',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.5',
      'version' => '6.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.8.3',
      'version' => '1.8.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '1afdd860a2566ed3c2b0b4a3de6e23434a79ec85',
    ),
    'icanboogie/inflector' => 
    array (
      'pretty_version' => 'v1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fb6ca7977f8a7ebb46aff0183ee868d798f77ab8',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => '00fc6afee788fa07c311b0650ad276585f8aef96',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df4af6a32908f1d89d74348624b57e3233eea247',
    ),
    'imgix/imgix-php' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6995ca3440fe442b1175c12dc63b41617568b606',
    ),
    'justinrainbow/json-schema' => 
    array (
      'pretty_version' => '5.2.11',
      'version' => '5.2.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '2ab6744b7296ded80f8cc4f9509abbff393399aa',
    ),
    'kornrunner/blurhash' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a09325353229c108c8d2ff129ec08b447753f9b',
    ),
    'kraken-io/kraken-php' => 
    array (
      'pretty_version' => '1.6',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '56563df1b9a7829f7039cc6e712cf89fab655e26',
    ),
    'ksubileau/color-thief-php' => 
    array (
      'pretty_version' => 'v1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc2acefacbd037f68cf61bcc62b30ac1bb16ed59',
    ),
    'laminas/laminas-escaper' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '25f2a053eadfa92ddacb609dcbbc39362610da70',
    ),
    'laminas/laminas-feed' => 
    array (
      'pretty_version' => '2.12.3',
      'version' => '2.12.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c91415633cb1be6f9d78683d69b7dcbfe6b4012',
    ),
    'laminas/laminas-stdlib' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2b18347625a2f06a1a485acfbc870f699dbe51c6',
    ),
    'laminas/laminas-zendframework-bridge' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ede70583e101030bcace4dcddd648f760ddf642',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.1.5',
      'version' => '1.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '18634df356bfd4119fe3d6156bdb990c414c14ea',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b38b25d7b372e9fddb00335400467b223349fd7e',
    ),
    'league/oauth2-client' => 
    array (
      'pretty_version' => '2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'badb01e62383430706433191b82506b6df24ad98',
    ),
    'league/oauth2-google' => 
    array (
      'pretty_version' => '3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '6b79441f244040760bed5fdcd092a2bda7cf34c6',
    ),
    'mapkit/jwt' => 
    array (
      'pretty_version' => 'v1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '19e502a991dbcb4becbcb51775b29aae624f551d',
    ),
    'masterminds/html5' => 
    array (
      'pretty_version' => '2.7.4',
      'version' => '2.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '9227822783c75406cfe400984b2f095cdf03d417',
    ),
    'maxmind-db/reader' => 
    array (
      'pretty_version' => 'v1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '569bd44d97d30a4ec12c7793a33004a76d4caf18',
    ),
    'maxmind/web-service-common' => 
    array (
      'pretty_version' => 'v0.8.1',
      'version' => '0.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '32f274051c543fc865e5a84d3a2c703913641ea8',
    ),
    'mikehaertl/php-shellcommand' => 
    array (
      'pretty_version' => '1.6.4',
      'version' => '1.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '3488d7803df1e8f1a343d3d0ca452d527ad8d5e5',
    ),
    'mmikkel/reasons' => 
    array (
      'pretty_version' => '2.2.5',
      'version' => '2.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '7de972929c7bab7e35de6173110af9a8daea4bd2',
    ),
    'mofodojodino/profanity-filter' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '529d24adcdb5b9507a4622db6aba6e54c5bb82a9',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.3.2',
      'version' => '2.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '71312564759a7db5b789296369c1a264efc43aad',
    ),
    'mtdowling/jmespath.php' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
    ),
    'mundschenk-at/php-typography' => 
    array (
      'pretty_version' => 'v6.6.0',
      'version' => '6.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '67af39c936751f96a959b9bf211044512675283c',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.46.0',
      'version' => '2.46.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2fd2c4a77d58a4e95234c8a61c5df1f157a91bf4',
    ),
    'nystudio107/craft-plugin-manifest' => 
    array (
      'pretty_version' => '1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd5c5b7c7b01e26eb6c8aabcaee27c6763cd6282b',
    ),
    'nystudio107/craft-seomatic' => 
    array (
      'pretty_version' => '3.4.6',
      'version' => '3.4.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bc43327cbf16bf138ce19151e410bd665d4b2f2b',
    ),
    'opis/closure' => 
    array (
      'pretty_version' => '3.6.2',
      'version' => '3.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '06e2ebd25f2869e54a306dda991f7db58066f7f6',
    ),
    'pacely/mailchimp-apiv3' => 
    array (
      'pretty_version' => 'v1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '859943e93fef53e93af1882faee5c1338a654c88',
    ),
    'paragonie/constant_time_encoding' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f34c2b11eb9d2c9318e13540a1dbc2a3afbd939c',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'php-science/textrank' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfa7480c9e136492109602473c242c1788279a4d',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '622548b623e81ca6d78b721c5e029f4ce664f170',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a12f7e301eb7258bb68acd89d4aefa05c2906cae',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => '1.7.5',
      'version' => '1.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '994ecccd8f3283ecf5ac33254543eb0ac946d525',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '3.0.10',
      'version' => '3.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '62fcc5a94ac83b1506f52d7558d828617fac9187',
    ),
    'pixelandtonic/imagine' => 
    array (
      'pretty_version' => '1.2.4.2',
      'version' => '1.2.4.2',
      'aliases' => 
      array (
      ),
      'reference' => '5ee4b6a365497818815ba50738c8dcbb555c9fd3',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0|2.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'putyourlightson/craft-sendgrid' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a544f3fe27697e2087fb18473439ec7c20226f43',
    ),
    'putyourlightson/craft-sprig' => 
    array (
      'pretty_version' => '1.10.4',
      'version' => '1.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '527b84b25fe94a5c51f98db8721f9d875e5d972f',
    ),
    'putyourlightson/craft-sprig-core' => 
    array (
      'pretty_version' => '1.1.6',
      'version' => '1.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a457a0a45296b4384442e64921af6cd2b4794ee',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'react/promise' => 
    array (
      'pretty_version' => 'v2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3cff96a19736714524ca0dd1d4130de73dbbbc4',
    ),
    'rize/uri-template' => 
    array (
      'pretty_version' => '0.3.3',
      'version' => '0.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e0b97e00e0f36c652dd3c37b194ef07de669b82',
    ),
    'seld/cli-prompt' => 
    array (
      'pretty_version' => '1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8dfcf02094b8c03b40322c229493bb2884423c5',
    ),
    'seld/jsonlint' => 
    array (
      'pretty_version' => '1.8.3',
      'version' => '1.8.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ad6ce79c342fbd44df10ea95511a1b24dee5b57',
    ),
    'seld/phar-utils' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '749042a2315705d2dfbbc59234dd9ceb22bf3ff0',
    ),
    'sendgrid/php-http-client' => 
    array (
      'pretty_version' => '3.14.0',
      'version' => '3.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7880d5aecc53856802130ba83af1dfcf942e9767',
    ),
    'sendgrid/sendgrid' => 
    array (
      'pretty_version' => '7.9.2',
      'version' => '7.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab0023a6233f39e408b5eb8c4299f20790f5f5a7',
    ),
    'sendgrid/sendgrid-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'spacecatninja/imager-x' => 
    array (
      'pretty_version' => 'v3.5.2',
      'version' => '3.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a851ecd1e66c9437949a11d048e3e8634d6d7b3',
    ),
    'spicyweb/craft-embedded-assets' => 
    array (
      'pretty_version' => '2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6246906f1f8c65d407ba0aeaa0ee3c7168425b44',
    ),
    'spicyweb/craft-neo' => 
    array (
      'pretty_version' => '2.11.9',
      'version' => '2.11.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '74eb82315f723047f5b19353927e7e3984e9a8e4',
    ),
    'ssnepenthe/color-utils' => 
    array (
      'pretty_version' => '0.4.2',
      'version' => '0.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a68562f81fd603be0c45d102b4e8064c76ddf863',
    ),
    'starkbank/ecdsa' => 
    array (
      'pretty_version' => '0.0.4',
      'version' => '0.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '9369d35ed9019321adb4eb9fd3be21357d527c74',
    ),
    'sunra/php-simple-html-dom-parser' => 
    array (
      'pretty_version' => 'v1.5.2',
      'version' => '1.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '75b9b1cb64502d8f8c04dc11b5906b969af247c6',
    ),
    'supercool/buttonbox' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '9123c54dfb5d8e536fafb47211ef4fdcfbd155f1',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.3.0',
      'version' => '6.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8a5d5072dca8f48460fce2f4131fcc495eec654c',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.3.10',
      'version' => '5.3.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd4e409d9fbcfbf71af0e5a940abb7b0b4bad0bd3',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v5.3.4',
      'version' => '5.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '343f4fe324383ca46792cae728a3b6e2f708fb32',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a10000ada1e600d109a6c7632e9ac42e8bf2fb93',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '63b5bb7db83e5673936d6e3b8b3e022ff6474933',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '16880ba9c5ebe3642d1995ab866db29270b36535',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '38f26c7d6ed535217ea393e05634cb0b244a1967',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.3.10',
      'version' => '5.3.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd70c35bb20bbca71fc4ab7921e3c6bda1a82a60c',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.2.6',
      'version' => '5.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '2cc7f45d96db9adfcf89adf4401d9dfed509f4e1',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.3.6',
      'version' => '5.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '4500fe63dc9c6ffc32d3b1cb0448c329f9c814b7',
    ),
    'theiconic/php-ga-measurement-protocol' => 
    array (
      'pretty_version' => 'v2.9.0',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6136c2f2ef159045402ef985843db0ad0f136125',
    ),
    'tinify/tinify' => 
    array (
      'pretty_version' => '1.5.2',
      'version' => '1.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b15d1f31d94d9b06e60251543cc918f426f0d55b',
    ),
    'topshelfcraft/wordsmith' => 
    array (
      'pretty_version' => '3.3.0.1',
      'version' => '3.3.0.1',
      'aliases' => 
      array (
      ),
      'reference' => 'e7acf27cc33e7fef8d53ab8af2d0ff498c424e67',
    ),
    'true/punycode' => 
    array (
      'pretty_version' => 'v2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a4d0c11a36dd7f4e7cd7096076cab6d3378a071e',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v2.14.7',
      'version' => '2.14.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e202327ee1ed863629de9b18a5ec70ac614d88f',
    ),
    'verbb/base' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '006cc4979e2b7669234a90bb7347129fe0185eb8',
    ),
    'verbb/field-manager' => 
    array (
      'pretty_version' => '2.2.4',
      'version' => '2.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '1e1085f4dd9cc8c244ff6b410584444a387d3b7e',
    ),
    'verbb/super-table' => 
    array (
      'pretty_version' => '2.6.8',
      'version' => '2.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a26d89e46c6a85a77177fdd3ec600a7e9d20efb5',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v3.6.8',
      'version' => '3.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e679f7616db829358341e2d5cccbd18773bdab8',
    ),
    'voku/anti-xss' => 
    array (
      'pretty_version' => '4.1.33',
      'version' => '4.1.33.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dcd44ad3726e97e3ea924e4c7257cb33976b71a5',
    ),
    'voku/arrayy' => 
    array (
      'pretty_version' => '7.8.13',
      'version' => '7.8.13.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ffaf679f3cb730c2706aa84be8c051e627999891',
    ),
    'voku/email-check' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ea842920bbef6758b8c1e619fd1710e7a1a2cac',
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '80953678b19901e5165c56752d087fc11526017c',
    ),
    'voku/portable-utf8' => 
    array (
      'pretty_version' => '5.4.51',
      'version' => '5.4.51.0',
      'aliases' => 
      array (
      ),
      'reference' => '578f5266725dc9880483d24ad0cfb39f8ce170f7',
    ),
    'voku/stop-words' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e63c0af20f800b1600783764e0ce19e53969f71',
    ),
    'voku/stringy' => 
    array (
      'pretty_version' => '6.4.1',
      'version' => '6.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b74e0e428e466bda91489b4c6e9ab385aa7c73a',
    ),
    'voku/urlify' => 
    array (
      'pretty_version' => '5.0.5',
      'version' => '5.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd59bfa6d13ce08062e2fe40dd23d226262f961c5',
    ),
    'weareferal/remote-core' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '351875a5d3612dcf32d568ac2d6ae7166e058084',
    ),
    'weareferal/remote-sync' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1f2f7bd00e6b01a99b02c2fba4da673dfed8258d',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
    'webonyx/graphql-php' => 
    array (
      'pretty_version' => 'v14.4.1',
      'version' => '14.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '769a47401f8cbc5f357ea4d05e0191e35a011b0f',
    ),
    'what3words/w3w-php-wrapper' => 
    array (
      'pretty_version' => 'v3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fc3c046378a463e2fd14d4002e620672590f7ef',
    ),
    'yii2tech/ar-softdelete' => 
    array (
      'pretty_version' => '1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '498ed03f89ded835f0ca156ec50d432191c58769',
    ),
    'yiisoft/yii2' => 
    array (
      'pretty_version' => '2.0.43',
      'version' => '2.0.43.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f370955faa3067d9b27879aaf14b0978a805cd59',
    ),
    'yiisoft/yii2-composer' => 
    array (
      'pretty_version' => '2.0.10',
      'version' => '2.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '94bb3f66e779e2774f8776d6e1bdeab402940510',
    ),
    'yiisoft/yii2-debug' => 
    array (
      'pretty_version' => '2.1.18',
      'version' => '2.1.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '45bc5d2ef4e3b0ef6f638190d42f04a77ab1df6c',
    ),
    'yiisoft/yii2-queue' => 
    array (
      'pretty_version' => '2.3.2',
      'version' => '2.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7446216c87eff371245113b41f5a227d4d9182f7',
    ),
    'yiisoft/yii2-swiftmailer' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '09659a55959f9e64b8178d842b64a9ffae42b994',
    ),
    'zendframework/zend-escaper' => 
    array (
      'replaced' => 
      array (
        0 => '2.6.1',
      ),
    ),
    'zendframework/zend-feed' => 
    array (
      'replaced' => 
      array (
        0 => '^2.12.0',
      ),
    ),
    'zendframework/zend-stdlib' => 
    array (
      'replaced' => 
      array (
        0 => '3.2.1',
      ),
    ),
  ),
);
    private static $canGetVendors;
    private static $installedByVendor = array();

    /**
     * Returns a list of all package names which are present, either by being installed, replaced or provided
     *
     * @return string[]
     * @psalm-return list<string>
     */
    public static function getInstalledPackages()
    {
        $packages = array();
        foreach (self::getInstalled() as $installed) {
            $packages[] = array_keys($installed['versions']);
        }

        if (1 === \count($packages)) {
            return $packages[0];
        }

        return array_keys(array_flip(\call_user_func_array('array_merge', $packages)));
    }

    /**
     * Checks whether the given package is installed
     *
     * This also returns true if the package name is provided or replaced by another package
     *
     * @param  string $packageName
     * @return bool
     */
    public static function isInstalled($packageName)
    {
        foreach (self::getInstalled() as $installed) {
            if (isset($installed['versions'][$packageName])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the given package satisfies a version constraint
     *
     * e.g. If you want to know whether version 2.3+ of package foo/bar is installed, you would call:
     *
     *   Composer\InstalledVersions::satisfies(new VersionParser, 'foo/bar', '^2.3')
     *
     * @param VersionParser $parser      Install composer/semver to have access to this class and functionality
     * @param string        $packageName
     * @param string|null   $constraint  A version constraint to check for, if you pass one you have to make sure composer/semver is required by your package
     *
     * @return bool
     */
    public static function satisfies(VersionParser $parser, $packageName, $constraint)
    {
        $constraint = $parser->parseConstraints($constraint);
        $provided = $parser->parseConstraints(self::getVersionRanges($packageName));

        return $provided->matches($constraint);
    }

    /**
     * Returns a version constraint representing all the range(s) which are installed for a given package
     *
     * It is easier to use this via isInstalled() with the $constraint argument if you need to check
     * whether a given version of a package is installed, and not just whether it exists
     *
     * @param  string $packageName
     * @return string Version constraint usable with composer/semver
     */
    public static function getVersionRanges($packageName)
    {
        foreach (self::getInstalled() as $installed) {
            if (!isset($installed['versions'][$packageName])) {
                continue;
            }

            $ranges = array();
            if (isset($installed['versions'][$packageName]['pretty_version'])) {
                $ranges[] = $installed['versions'][$packageName]['pretty_version'];
            }
            if (array_key_exists('aliases', $installed['versions'][$packageName])) {
                $ranges = array_merge($ranges, $installed['versions'][$packageName]['aliases']);
            }
            if (array_key_exists('replaced', $installed['versions'][$packageName])) {
                $ranges = array_merge($ranges, $installed['versions'][$packageName]['replaced']);
            }
            if (array_key_exists('provided', $installed['versions'][$packageName])) {
                $ranges = array_merge($ranges, $installed['versions'][$packageName]['provided']);
            }

            return implode(' || ', $ranges);
        }

        throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
    }

    /**
     * @param  string      $packageName
     * @return string|null If the package is being replaced or provided but is not really installed, null will be returned as version, use satisfies or getVersionRanges if you need to know if a given version is present
     */
    public static function getVersion($packageName)
    {
        foreach (self::getInstalled() as $installed) {
            if (!isset($installed['versions'][$packageName])) {
                continue;
            }

            if (!isset($installed['versions'][$packageName]['version'])) {
                return null;
            }

            return $installed['versions'][$packageName]['version'];
        }

        throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
    }

    /**
     * @param  string      $packageName
     * @return string|null If the package is being replaced or provided but is not really installed, null will be returned as version, use satisfies or getVersionRanges if you need to know if a given version is present
     */
    public static function getPrettyVersion($packageName)
    {
        foreach (self::getInstalled() as $installed) {
            if (!isset($installed['versions'][$packageName])) {
                continue;
            }

            if (!isset($installed['versions'][$packageName]['pretty_version'])) {
                return null;
            }

            return $installed['versions'][$packageName]['pretty_version'];
        }

        throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
    }

    /**
     * @param  string      $packageName
     * @return string|null If the package is being replaced or provided but is not really installed, null will be returned as reference
     */
    public static function getReference($packageName)
    {
        foreach (self::getInstalled() as $installed) {
            if (!isset($installed['versions'][$packageName])) {
                continue;
            }

            if (!isset($installed['versions'][$packageName]['reference'])) {
                return null;
            }

            return $installed['versions'][$packageName]['reference'];
        }

        throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
    }

    /**
     * @return array
     * @psalm-return array{name: string, version: string, reference: string, pretty_version: string, aliases: string[]}
     */
    public static function getRootPackage()
    {
        $installed = self::getInstalled();

        return $installed[0]['root'];
    }

    /**
     * Returns the raw installed.php data for custom implementations
     *
     * @return array[]
     * @psalm-return array{root: array{name: string, version: string, reference: string, pretty_version: string, aliases: string[]}, versions: array<string, array{pretty_version?: string, version?: string, aliases?: string[], reference?: string, replaced?: string[], provided?: string[]}>}
     */
    public static function getRawData()
    {
        return self::$installed;
    }

    /**
     * Lets you reload the static array from another file
     *
     * This is only useful for complex integrations in which a project needs to use
     * this class but then also needs to execute another project's autoloader in process,
     * and wants to ensure both projects have access to their version of installed.php.
     *
     * A typical case would be PHPUnit, where it would need to make sure it reads all
     * the data it needs from this class, then call reload() with
     * `require $CWD/vendor/composer/installed.php` (or similar) as input to make sure
     * the project in which it runs can then also use this class safely, without
     * interference between PHPUnit's dependencies and the project's dependencies.
     *
     * @param  array[] $data A vendor/composer/installed.php data set
     * @return void
     *
     * @psalm-param array{root: array{name: string, version: string, reference: string, pretty_version: string, aliases: string[]}, versions: array<string, array{pretty_version?: string, version?: string, aliases?: string[], reference?: string, replaced?: string[], provided?: string[]}>} $data
     */
    public static function reload($data)
    {
        self::$installed = $data;
        self::$installedByVendor = array();
    }

    /**
     * @return array[]
     * @psalm-return list<array{root: array{name: string, version: string, reference: string, pretty_version: string, aliases: string[]}, versions: array<string, array{pretty_version?: string, version?: string, aliases?: string[], reference?: string, replaced?: string[], provided?: string[]}>}>
     */
    private static function getInstalled()
    {
        if (null === self::$canGetVendors) {
            self::$canGetVendors = method_exists('Composer\Autoload\ClassLoader', 'getRegisteredLoaders');
        }

        $installed = array();

        if (self::$canGetVendors) {
            foreach (ClassLoader::getRegisteredLoaders() as $vendorDir => $loader) {
                if (isset(self::$installedByVendor[$vendorDir])) {
                    $installed[] = self::$installedByVendor[$vendorDir];
                } elseif (is_file($vendorDir.'/composer/installed.php')) {
                    $installed[] = self::$installedByVendor[$vendorDir] = require $vendorDir.'/composer/installed.php';
                }
            }
        }

        $installed[] = self::$installed;

        return $installed;
    }
}

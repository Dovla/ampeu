<?php

$vendorDir = dirname(__DIR__);
$rootDir = dirname(dirname(__DIR__));

return array (
  'topshelfcraft/wordsmith' => 
  array (
    'class' => 'topshelfcraft\\wordsmith\\Wordsmith',
    'basePath' => $vendorDir . '/topshelfcraft/wordsmith/src',
    'handle' => 'wordsmith',
    'aliases' => 
    array (
      '@topshelfcraft/wordsmith' => $vendorDir . '/topshelfcraft/wordsmith/src',
    ),
    'name' => 'Wordsmith',
    'version' => '3.3.0.1',
    'schemaVersion' => '0.0.0.0',
    'description' => '...because you have the best words.',
    'developer' => 'Michael Rog',
    'developerUrl' => 'https://topshelfcraft.com',
    'documentationUrl' => 'https://wordsmith.docs.topshelfcraft.com/',
    'changelogUrl' => 'https://raw.githubusercontent.com/topshelfcraft/wordsmith/master/CHANGELOG.md',
  ),
  'fruitstudios/linkit' => 
  array (
    'class' => 'fruitstudios\\linkit\\Linkit',
    'basePath' => $vendorDir . '/fruitstudios/linkit/src',
    'handle' => 'linkit',
    'aliases' => 
    array (
      '@fruitstudios/linkit' => $vendorDir . '/fruitstudios/linkit/src',
    ),
    'name' => 'Linkit',
    'version' => '1.1.12.1',
    'description' => 'One link field to rule them all.',
    'developer' => 'Fruit Studios',
    'developerUrl' => 'https://fruitstudios.co.uk',
    'developerEmail' => 'hi@fruitstudios.co.uk',
    'documentationUrl' => 'https://github.com/fruitstudios/craft-linkit',
    'changelogUrl' => 'https://raw.githubusercontent.com/fruitstudios/craft-linkit/master/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => false,
    'components' => 
    array (
      'services' => 'fruitstudios\\linkit\\services\\LinkitService',
    ),
  ),
  'aelvan/mailchimp-subscribe' => 
  array (
    'class' => 'aelvan\\mailchimpsubscribe\\MailchimpSubscribe',
    'basePath' => $vendorDir . '/aelvan/mailchimp-subscribe/src',
    'handle' => 'mailchimp-subscribe',
    'aliases' => 
    array (
      '@aelvan/mailchimpsubscribe' => $vendorDir . '/aelvan/mailchimp-subscribe/src',
    ),
    'name' => 'Mailchimp Subscribe',
    'version' => '3.0.3',
    'schemaVersion' => '2.0.0',
    'description' => 'Simple Craft plugin for subscribing to a MailChimp audience.',
    'developer' => 'André Elvan',
    'developerUrl' => 'https://www.vaersaagod.no',
    'documentationUrl' => 'https://github.com/aelvan/mailchimp-subscribe-craft/blob/craft3/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/aelvan/mailchimp-subscribe/master/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => false,
    'components' => 
    array (
    ),
  ),
  'putyourlightson/craft-sendgrid' => 
  array (
    'class' => 'putyourlightson\\sendgrid\\Sendgrid',
    'basePath' => $vendorDir . '/putyourlightson/craft-sendgrid/src',
    'handle' => 'sendgrid',
    'aliases' => 
    array (
      '@putyourlightson/sendgrid' => $vendorDir . '/putyourlightson/craft-sendgrid/src',
    ),
    'name' => 'SendGrid',
    'version' => '1.2.2',
    'schemaVersion' => '1.0.0',
    'description' => 'SendGrid mailer adapter for Craft CMS.',
    'developer' => 'PutYourLightsOn',
    'developerUrl' => 'https://putyourlightson.com/',
    'documentationUrl' => 'https://putyourlightson.com/plugins/sendgrid',
    'changelogUrl' => 'https://raw.githubusercontent.com/putyourlightson/craft-sendgrid/v1/CHANGELOG.md',
  ),
  'craftcms/redactor' => 
  array (
    'class' => 'craft\\redactor\\Plugin',
    'basePath' => $vendorDir . '/craftcms/redactor/src',
    'handle' => 'redactor',
    'aliases' => 
    array (
      '@craft/redactor' => $vendorDir . '/craftcms/redactor/src',
    ),
    'name' => 'Redactor',
    'version' => '2.8.8',
    'description' => 'Edit rich text content in Craft CMS using Redactor by Imperavi.',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/redactor/blob/v2/README.md',
  ),
  'verbb/super-table' => 
  array (
    'class' => 'verbb\\supertable\\SuperTable',
    'basePath' => $vendorDir . '/verbb/super-table/src',
    'handle' => 'super-table',
    'aliases' => 
    array (
      '@verbb/supertable' => $vendorDir . '/verbb/super-table/src',
    ),
    'name' => 'Super Table',
    'version' => '2.6.8',
    'description' => 'Super-charge your Craft workflow with Super Table. Use it to group fields together or build complex Matrix-in-Matrix solutions.',
    'developer' => 'Verbb',
    'developerUrl' => 'https://verbb.io',
    'developerEmail' => 'support@verbb.io',
    'documentationUrl' => 'https://github.com/verbb/super-table',
    'changelogUrl' => 'https://raw.githubusercontent.com/verbb/super-table/craft-3/CHANGELOG.md',
  ),
  'spicyweb/craft-embedded-assets' => 
  array (
    'class' => 'spicyweb\\embeddedassets\\Plugin',
    'basePath' => $vendorDir . '/spicyweb/craft-embedded-assets/src',
    'handle' => 'embeddedassets',
    'aliases' => 
    array (
      '@spicyweb/embeddedassets' => $vendorDir . '/spicyweb/craft-embedded-assets/src',
    ),
    'name' => 'Embedded Assets',
    'version' => '2.8.0',
    'schemaVersion' => '1.0.0',
    'description' => 'Manage YouTube videos, Instagram photos, Twitter posts and more as first class assets',
    'developer' => 'Spicy Web',
    'developerUrl' => 'https://spicyweb.com.au',
    'documentationUrl' => 'https://github.com/spicywebau/craft-embedded-assets/blob/2.8.0/README.md',
    'changelogUrl' => 'https://github.com/spicywebau/craft-embedded-assets/blob/master/CHANGELOG.md',
    'downloadUrl' => 'https://github.com/spicywebau/craft-embedded-assets/archive/master.zip',
  ),
  'dukt/analytics' => 
  array (
    'class' => 'dukt\\analytics\\Plugin',
    'basePath' => $vendorDir . '/dukt/analytics/src',
    'handle' => 'analytics',
    'aliases' => 
    array (
      '@dukt/analytics' => $vendorDir . '/dukt/analytics/src',
    ),
    'name' => 'Analytics',
    'version' => '4.0.13',
    'schemaVersion' => '1.1.2',
    'description' => 'Customizable statistics widgets and entry tracking for Google Analytics.',
    'developer' => 'Dukt',
    'developerUrl' => 'https://dukt.net/',
    'developerEmail' => 'support@dukt.net',
    'documentationUrl' => 'https://dukt.net/docs/analytics/v4',
    'changelogUrl' => 'https://raw.githubusercontent.com/dukt/analytics/develop/CHANGELOG.md',
  ),
  'weareferal/remote-sync' => 
  array (
    'class' => 'weareferal\\remotesync\\RemoteSync',
    'basePath' => $vendorDir . '/weareferal/remote-sync/src',
    'handle' => 'remote-sync',
    'aliases' => 
    array (
      '@weareferal/remotesync' => $vendorDir . '/weareferal/remote-sync/src',
    ),
    'name' => 'Remote Sync',
    'version' => '1.4.0',
    'description' => 'Sync your database and assets across Craft environments',
    'developer' => 'Timmy O\'Mahony',
    'developerUrl' => 'https://weareferal.com',
    'documentationUrl' => 'https://github.com/weareferal/craft-remote-sync/blob/master/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/weareferal/craft-remote-sync/master/CHANGELOG.md',
    'hasCpSettings' => true,
    'hasCpSection' => false,
  ),
  'spacecatninja/imager-x' => 
  array (
    'class' => 'spacecatninja\\imagerx\\ImagerX',
    'basePath' => $vendorDir . '/spacecatninja/imager-x/src',
    'handle' => 'imager-x',
    'aliases' => 
    array (
      '@spacecatninja/imagerx' => $vendorDir . '/spacecatninja/imager-x/src',
    ),
    'name' => 'Imager X',
    'version' => 'v3.5.2',
    'schemaVersion' => '3.0.0',
    'description' => 'Ninja powered image transforms.',
    'developer' => 'SPACECATNINJA',
    'developerUrl' => 'https://www.spacecat.ninja',
    'documentationUrl' => 'http://imager-x.spacecat.ninja/',
    'changelogUrl' => 'https://raw.githubusercontent.com/spacecatninja/craft-imager-x/master/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => false,
    'components' => 
    array (
    ),
  ),
  'verbb/field-manager' => 
  array (
    'class' => 'verbb\\fieldmanager\\FieldManager',
    'basePath' => $vendorDir . '/verbb/field-manager/src',
    'handle' => 'field-manager',
    'aliases' => 
    array (
      '@verbb/fieldmanager' => $vendorDir . '/verbb/field-manager/src',
    ),
    'name' => 'Field Manager',
    'version' => '2.2.4',
    'description' => 'Manage your fields and field groups with ease with simple field or group cloning and quicker overall management.',
    'developer' => 'Verbb',
    'developerUrl' => 'https://verbb.io',
    'developerEmail' => 'support@verbb.io',
    'documentationUrl' => 'https://github.com/verbb/field-manager',
    'changelogUrl' => 'https://raw.githubusercontent.com/verbb/field-manager/craft-3/CHANGELOG.md',
  ),
  'ether/simplemap' => 
  array (
    'class' => 'ether\\simplemap\\SimpleMap',
    'basePath' => $vendorDir . '/ether/simplemap/src',
    'handle' => 'simplemap',
    'aliases' => 
    array (
      '@ether/simplemap' => $vendorDir . '/ether/simplemap/src',
    ),
    'name' => 'Maps',
    'version' => '3.9.2',
    'schemaVersion' => '3.4.2',
    'description' => 'A beautifully simple Map field type for Craft CMS 3',
    'developer' => 'Ether Creative',
    'developerUrl' => 'https://ethercreative.co.uk',
    'developerEmail' => 'help@ethercreative.co.uk',
    'documentationUrl' => 'https://docs.ethercreative.co.uk/maps',
  ),
  'nystudio107/craft-seomatic' => 
  array (
    'class' => 'nystudio107\\seomatic\\Seomatic',
    'basePath' => $vendorDir . '/nystudio107/craft-seomatic/src',
    'handle' => 'seomatic',
    'aliases' => 
    array (
      '@nystudio107/seomatic' => $vendorDir . '/nystudio107/craft-seomatic/src',
    ),
    'name' => 'SEOmatic',
    'version' => '3.4.6',
    'description' => 'SEOmatic facilitates modern SEO best practices & implementation for Craft CMS 3. It is a turnkey SEO system that is comprehensive, powerful, and flexible.',
    'developer' => 'nystudio107',
    'developerUrl' => 'https://nystudio107.com',
    'documentationUrl' => 'https://nystudio107.com/plugins/seomatic/documentation',
    'changelogUrl' => 'https://raw.githubusercontent.com/nystudio107/craft-seomatic/v3/CHANGELOG.md',
  ),
  'spicyweb/craft-neo' => 
  array (
    'class' => 'benf\\neo\\Plugin',
    'basePath' => $vendorDir . '/spicyweb/craft-neo/src',
    'handle' => 'neo',
    'aliases' => 
    array (
      '@benf/neo' => $vendorDir . '/spicyweb/craft-neo/src',
    ),
    'name' => 'Neo',
    'version' => '2.11.9',
    'schemaVersion' => '2.11.6',
    'description' => 'A Matrix-like field type that uses existing fields',
    'developer' => 'Spicy Web',
    'developerUrl' => 'https://github.com/spicywebau',
    'documentationUrl' => 'https://github.com/spicywebau/craft-neo/blob/2.11.9/README.md',
    'changelogUrl' => 'https://github.com/spicywebau/craft-neo/blob/master/CHANGELOG.md',
    'downloadUrl' => 'https://github.com/spicywebau/craft-neo/archive/master.zip',
  ),
  'mmikkel/reasons' => 
  array (
    'class' => 'mmikkel\\reasons\\Reasons',
    'basePath' => $vendorDir . '/mmikkel/reasons/src',
    'handle' => 'reasons',
    'aliases' => 
    array (
      '@mmikkel/reasons' => $vendorDir . '/mmikkel/reasons/src',
    ),
    'name' => 'Reasons',
    'version' => '2.2.5',
    'description' => 'Adds conditionals to field layouts.',
    'developer' => 'Mats Mikkel Rummelhoff',
    'developerUrl' => 'https://vaersaagod.no',
    'documentationUrl' => 'https://github.com/mmikkel/Reasons-Craft3/blob/master/README.md',
    'changelogUrl' => 'https://github.com/mmikkel/Reasons-Craft3/blob/master/CHANGELOG.md',
  ),
  'elleracompany/craft-cookie-consent' => 
  array (
    'class' => 'elleracompany\\cookieconsent\\CookieConsent',
    'basePath' => $vendorDir . '/elleracompany/craft-cookie-consent/src',
    'handle' => 'cookie-consent',
    'aliases' => 
    array (
      '@elleracompany/cookieconsent' => $vendorDir . '/elleracompany/craft-cookie-consent/src',
    ),
    'name' => 'GDPR Cookie Consent',
    'version' => '1.6.0',
    'description' => 'GDPR Compliant Cookie Consent Plugin',
    'developer' => 'Ellera AS',
    'developerUrl' => 'https://ellera.no',
    'developerEmail' => 'support@ellera.no',
    'documentationUrl' => 'https://github.com/elleracompany/craft-cookie-consent/blob/master/README.md',
    'changelogUrl' => 'https://github.com/elleracompany/craft-cookie-consent/blob/master/CHANGELOG.md',
  ),
  'supercool/buttonbox' => 
  array (
    'class' => 'supercool\\buttonbox\\ButtonBox',
    'basePath' => $vendorDir . '/supercool/buttonbox/src',
    'handle' => 'buttonbox',
    'aliases' => 
    array (
      '@supercool/buttonbox' => $vendorDir . '/supercool/buttonbox/src',
    ),
    'name' => 'Button Box',
    'version' => '2.0.4',
    'schemaVersion' => '1.0.0',
    'description' => 'A collection of utility field types for Craft',
    'developer' => 'Supercool',
    'developerUrl' => 'http://supercooldesign.co.uk',
    'documentationUrl' => 'https://github.com/supercool/buttonbox/blob/master/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/supercool/buttonbox/master/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => false,
  ),
  'putyourlightson/craft-sprig' => 
  array (
    'class' => 'putyourlightson\\sprig\\plugin\\Sprig',
    'basePath' => $vendorDir . '/putyourlightson/craft-sprig/src',
    'handle' => 'sprig',
    'aliases' => 
    array (
      '@putyourlightson/sprig/plugin' => $vendorDir . '/putyourlightson/craft-sprig/src',
    ),
    'name' => 'Sprig',
    'version' => '1.10.4',
    'schemaVersion' => '1.0.1',
    'description' => 'A reactive Twig component framework for Craft.',
    'developer' => 'PutYourLightsOn',
    'developerUrl' => 'https://putyourlightson.com/',
    'documentationUrl' => 'https://putyourlightson.com/plugins/sprig',
    'changelogUrl' => 'https://raw.githubusercontent.com/putyourlightson/craft-sprig/v1/CHANGELOG.md',
  ),
);

# Static Analysis

To run static analysis on the plugin, install PHPStan and run the following command from the root of your project.

    vendor/bin/phpstan analyse vendor/putyourlightson/craft-sprig-core/src -c vendor/putyourlightson/craft-sprig-core/phpstan.neon -l 3

# Testing

To test the plugin, install Codeception, update `.env` and run the following command from the root of your project.

    ./vendor/bin/codecept run -c ./vendor/putyourlightson/craft-sprig-core unit

Or to run a specific test.

    ./vendor/bin/codecept run -c ./vendor/putyourlightson/craft-sprig-core unit services/ComponentsTest:create

> Ensure that the database you specify in `.env` is not one that actually contains any data as it will be cleared when the tests are run. 

<?php
//
// inc_newsletter_form_processing.php

require_once('inc/class.nlform.php');


$nlError = array();
$nlMessage = array();

$countiesReq = nlform::counties();
$counties = array();

/*
counties:
 [value] => Array ( [0] => Array ( [id] => -7 [code] => 7 [name] => Bjelovarsko-bilogorska [description] => [status] => 1 )

*/


if ($countiesReq['error'] == '') {
	if (isset($countiesReq['content'])) {
		if (isset($countiesReq['content']['value'])) {
			if (is_array($countiesReq['content']['value'])) {
				$counties = $countiesReq['content']['value'];
			} else {
				$nlError[] = 'Greška prilikom dohvaćanja županija: "value" nije niz!';
			}
		} else {
			$nlError[] = 'Greška prilikom dohvaćanja županija: nema vrijednosti';
			if (isset($countiesReq['content']['message']) && ($countiesReq['content']['message'] != "")) {
				$nlError[] = $countiesReq['content']['message'];
			}
		}
	} else {
		$nlError[] = 'Greška prilikom dohvaćanja županija: nema sadržaja';
	}
} else {
	$nlError[] = 'Greška prilikom dohvaćanja županija: '.$countiesReq['error'];

}

$interestsReq = nlform::fieldOfInterest();
$interests = array();
//print_r($interestsReq);

/*
 [status] => SUCCESS [message] => [value] => Array ( ) [requestId] => 65033d52f34c40e69cc7414e9b73525a ) )

*/

if ($interestsReq['error'] == '') {
	if (isset($interestsReq['content'])) {
		if (isset($interestsReq['content']['value'])) {
			if (is_array($interestsReq['content']['value'])) {
				$interests = $interestsReq['content']['value'];
			} else {
				$nlError[] = 'Greška prilikom dohvaćanja polja interesa: "value" nije niz!';
			}
		} else {
			$nlError[] = 'Greška prilikom dohvaćanja polja interesa: nema vrijednosti';
			if (isset($interestsReq['content']['message']) && ($interestsReq['content']['message'] != "")) {
				$nlError[] = $interestsReq['content']['message'];
			}
		}
	} else {
		$nlError[] = 'Greška prilikom dohvaćanja polja interesa: nema sadržaja';
	}
} else {
	$nlError[] = 'Greška prilikom dohvaćanja polja interesa: '.$interestsReq['error'];

}


$professionalDevelopmentActivityReq = nlform::professionalDevelopmentActivity();
$professionalDevelopmentActivity = array();
//print_r($professionalDevelopmentActivityReq);
if ($professionalDevelopmentActivityReq['error'] == '') {
	if (isset($professionalDevelopmentActivityReq['content'])) {
		if (isset($professionalDevelopmentActivityReq['content']['value'])) {
			if (is_array($professionalDevelopmentActivityReq['content']['value'])) {
				$professionalDevelopmentActivity = $professionalDevelopmentActivityReq['content']['value'];
			} else {
				$nlError[] = 'Greška prilikom dohvaćanja aktivnosti profesionalnog usavršavanja: "value" nije niz!';
			}
		} else {
			$nlError[] = 'Greška prilikom dohvaćanja aktivnosti profesionalnog usavršavanja: nema vrijednosti';
			if (isset($professionalDevelopmentActivityReq['content']['message']) && ($professionalDevelopmentActivityReq['content']['message'] != "")) {
				$nlError[] = $professionalDevelopmentActivityReq['content']['message'];
			}
		}
	} else {
		$nlError[] = 'Greška prilikom dohvaćanja aktivnosti profesionalnog usavršavanja: nema sadržaja';
	}
} else {
	$nlError[] = 'Greška prilikom dohvaćanja aktivnosti profesionalnog usavršavanja: '.$professionalDevelopmentActivityReq['error'];

}


$euroguidanceProfileReq = nlform::euroguidanceProfiles();
$euroguidanceProfile = array();
//print_r($euroguidanceProfileReq);
if ($euroguidanceProfileReq['error'] == '') {
	if (isset($euroguidanceProfileReq['content'])) {
		if (isset($euroguidanceProfileReq['content']['value'])) {
			if (is_array($euroguidanceProfileReq['content']['value'])) {
				$euroguidanceProfile = $euroguidanceProfileReq['content']['value'];
			} else {
				$nlError[] = 'Greška prilikom dohvaćanja euroguidance profila: "value" nije niz!';
			}
		} else {
			$nlError[] = 'Greška prilikom dohvaćanja euroguidance profila: nema vrijednosti';
			if (isset($euroguidanceProfileReq['content']['message']) && ($euroguidanceProfileReq['content']['message'] != "")) {
				$nlError[] = $euroguidanceProfileReq['content']['message'];
			}
		}
	} else {
		$nlError[] = 'Greška prilikom dohvaćanja euroguidance profila: nema sadržaja';
	}
} else {
	$nlError[] = 'Greška prilikom dohvaćanja euroguidance profila: '.$euroguidanceProfileReq['error'];

}



$formSubmitOk = false;
$nullSubmit = false;

//
// form submit start
//
if (isset($_POST['nl_reg_form']) && ($_POST['nl_reg_form'] == 'submit') ) {

	

	/*
	* 2021-06-08 field organizacija_mjesto
	* LocationId removed, location is free input field
	* LocationName added
	*
	* 2021-06-25 field
	*/

	// post data init for sending
	$remotePostData = array(
		'firstName'=>'',
		'lastName'=>'',
		'email'=>'',
		'organizationType' => '',
		'organizationName'=>'',
		'countyId'=>0,
		'LocationName'=>'',
		'address'=>'',
		'personRole'=>'',
		'fieldsOfInterest'=>array(),
		'professionalDevelopmentActivityId'=>0,
		'euroguidanceProfileId'=>0,

	);

	$checkEuroguidance = false;
	$checkEtwinning = false;
/*
	if (count($nlError) == 0) {
		if (!isset($_POST['ime']) || (trim($_POST['ime']) == '')) {
			$nlError[] = 'Ime je obavezno';
		}
	}

	if (count($nlError) == 0) {
		if (!isset($_POST['prezime']) || (trim($_POST['prezime']) == '')) {
			$nlError[] = 'Prezime je obavezno';
		}
	}
*/
	if (count($nlError) == 0) {
		if (!isset($_POST['email']) || (trim($_POST['email']) == '')) {
			$nlError[] = 'Email adresa je obavezna';
		}
	}

	if (count($nlError) == 0) {
		if (nlform::checkEmail(trim($_POST['email'])) != true) {
			$nlError[] = 'Email adresa nije ispravna';
		}
	}

	if (count($nlError) == 0) {
		if (!isset($_POST['organizacija_vrsta']) || (trim($_POST['organizacija_vrsta']) == '')) {
			$nlError[] = 'Unesite vrstu organizacije';
		}
	}

	if (count($nlError) == 0) {
		if (!isset($_POST['organizacija_naziv']) || (trim($_POST['organizacija_naziv']) == '')) {
			$nlError[] = 'Unesite naziv organizacije';
		}
	}
/*
	if (count($nlError) == 0) {
		if (!isset($_POST['organizacija_zupanija']) || (trim($_POST['organizacija_zupanija']) == '')) {
			$nlError[] = 'Odaberite županiju organizacije';
		}
	}

	if (count($nlError) == 0) {
		if (!isset($_POST['organizacija_mjesto']) || (trim($_POST['organizacija_mjesto']) == '')) {
			$nlError[] = 'Unesite mjesto organizacije';
		}
	}

	if (count($nlError) == 0) {
		if (!isset($_POST['organizacija_adresa']) || (trim($_POST['organizacija_adresa']) == '')) {
			$nlError[] = 'Unesite adresu organizacije';
		}
	}

	if (count($nlError) == 0) {
		if (!isset($_POST['funkcija']) || (trim($_POST['funkcija']) == '')) {
			$nlError[] = 'Unesite funkciju';
		}
	}
*/
	if (count($nlError) == 0) {
		if (! (isset($_POST['podrucje_interesa']) && (is_array($_POST['podrucje_interesa'])) && (count($_POST['podrucje_interesa']) > 0)) ) {
			$nlError[] = 'Odaberite područje interesa';
		}
	}

	$fieldsOfInterest = array();
	if (count($nlError) == 0) {
		foreach ($_POST['podrucje_interesa'] as $_input) {
			if (trim($_input) != "") {
				$fieldsOfInterest[] = nlform::filterInput(nlform::stripWordHtml(trim($_input), ''));
				//Euroguidance
				if ($_input == '-14') {
					$checkEuroguidance = true;
				}
				//  -11
				if ($_input == '-11') {
					// etwinning additional fields removed
					//$checkEtwinning = true;
				}
			}
		}
	}

	// euroguidance fields

	$professionalDevelopmentActivityId = 0;
	if (count($nlError) == 0) {
		if ($checkEuroguidance == true) {
			if (isset($_POST['profesionalno_usavrsavanje'])) {
				if ((trim($_POST['profesionalno_usavrsavanje']) == '')) {
					$nlError[] = 'Odaberite aktivnosti profesionalnog usavršavanja';
				} else {
					$professionalDevelopmentActivityId = nlform::filterInput(nlform::stripWordHtml(trim($_POST['profesionalno_usavrsavanje']), ''));
				}
			} else {
				$nlError[] = 'Odaberite aktivnosti profesionalnog usavršavanja';
			}
		}
	}

	$euroguidanceProfileId = 0;
	if (count($nlError) == 0) {
		if ($checkEuroguidance == true) {
			if (isset($_POST['euroguidance_profil'])) {
				if ((trim($_POST['euroguidance_profil']) == '')) {
					$nlError[] = 'Odaberite profil za Euroguidance';
				} else {
					$euroguidanceProfileId = nlform::filterInput(nlform::stripWordHtml(trim($_POST['euroguidance_profil']), ''));
				}
			} else {
				$nlError[] = 'Odaberite aktivnosti profesionalnog usavršavanja';
			}
		}
	}

	//etwinning fields
	// 2021-06-28 removed
	/*
	$etwinningSubject = '';
	if (count($nlError) == 0) {
		if ($checkEtwinning == true) {
			if (isset($_POST['etwinning_predmet'])) {
				if ((trim($_POST['etwinning_predmet']) == '')) {
					$nlError[] = 'Odaberite predmet za eTwinning';
				} else {
					$etwinningSubject = nlform::filterInput(nlform::stripWordHtml(trim($_POST['etwinning_predmet']), ''));
				}
			} else {
				$nlError[] = 'Odaberite predmet za eTwinning';
			}
		}
	}

	$eTwinningFieldOfInterest = '';
	if (count($nlError) == 0) {
		if ($checkEtwinning == true) {
			if (isset($_POST['etwinning_podrucje_interesa'])) {
				if ((trim($_POST['etwinning_podrucje_interesa']) == '')) {
					$nlError[] = 'Unesite područje interesa za eTwinning';
				} else {
					$eTwinningFieldOfInterest = nlform::filterInput(nlform::stripWordHtml(trim($_POST['etwinning_podrucje_interesa']), ''));
				}
			} else {
				$nlError[] = 'Unesite područje interesa za eTwinning';
			}
		}
	}

	$eTwinningCountyIds = array();
	if (count($nlError) == 0) {
		if ($checkEtwinning == true) {
			if (isset($_POST['etwinning_zupanija'])) {
				if (! ((is_array($_POST['etwinning_zupanija'])) && (count($_POST['etwinning_zupanija']) > 0)) ) {
					$nlError[] = 'Odaberite županiju eTwinninga';
				} else {
					foreach ($_POST['etwinning_zupanija'] as $_input) {
						if (trim($_input) != "") {
							$eTwinningCountyIds[] = nlform::filterInput(nlform::stripWordHtml(trim($_input), ''));
						}
					}

				}
			} else {
				$nlError[] = 'Odaberite županiju eTwinninga';
			}
		}
	}
	*/

	// bad people filtering
	if (count($nlError) == 0) {
		if (strpos($_POST['email'], 'mail.ru') !== false) {
			$nullSubmit = true;
		}
		if (strpos($_POST['email'], 'yandex.ru') !== false) {
			$nullSubmit = true;
		}
		if (strpos($_POST['ime'], 'http') !== false) {
			$nullSubmit = true;
		}
		if (strpos($_POST['prezime'], 'http') !== false) {
			$nullSubmit = true;
		}
	}




	if (count($nlError) == 0) {

		//$formSubmitOk = true;


		$remotePostData = array(
			'firstName'=>nlform::filterInput(nlform::stripWordHtml(trim($_POST['ime']), '')),
			'lastName'=>nlform::filterInput(nlform::stripWordHtml(trim($_POST['prezime']), '')),
			'email'=>trim($_POST['email']),
			'organizationType' => nlform::filterInput(nlform::stripWordHtml(trim($_POST['organizacija_vrsta']), '')),
			'organizationName'=>nlform::filterInput(nlform::stripWordHtml(trim($_POST['organizacija_naziv']), '')),
			'countyId'=>nlform::filterInput(nlform::stripWordHtml(trim($_POST['organizacija_zupanija']), '')),
			'locationName'=>nlform::filterInput(nlform::stripWordHtml(trim($_POST['organizacija_mjesto']), '')),
			'address'=>nlform::filterInput(nlform::stripWordHtml(trim($_POST['organizacija_adresa']), '')),
			'personRole'=>nlform::filterInput(nlform::stripWordHtml(trim($_POST['funkcija']), '')),
			'fieldsOfInterest'=>$fieldsOfInterest,
			'professionalDevelopmentActivityId'=>$professionalDevelopmentActivityId,
			'euroguidanceProfileId'=>$euroguidanceProfileId
		);

		if ($nullSubmit == false) {

			$postResult = nlform::doHttpRequest ('http://193.198.64.74:5100/api/provisioning/registerperson', 'POST', array(), $remotePostData);
			// test
			/*
			echo 'form ok, registering person<br>';
			print_r($remotePostData);
			$formSubmitOk = true;
			*/

		} else {
			$formSubmitOk = true;
		}
		// postResult OK:

		if ($nullSubmit == false) {
			if (isset($postResult['result_code']) && ($postResult['result_code'] == 200)) {
				$formSubmitOk = true;
			} else {
				$nlError[] = 'Greška prilikom spremanja podataka:';
				if (isset($postResult['content']['status'])) {
					$nlError[] = 'status: '.$postResult['content']['status'];
				}
				$nlError[] = print_r($postResult['content'], true);
			}
		}


	}


} // END if (isset($_POST['nl_reg_form']) && ($_POST['nl_reg_form'] == 'submit') ) {




//
// end include
//

?>
<?php
	// ====================================
	// NEWSLETTER REG FORM START             =
	// ==================================== 

	 
	?>
		<div class="newsletter_from_wrap pt-4 pb-2">
		<?php
		if (isset($nlError) && (is_array($nlError)) && (count($nlError) > 0)) {
			echo '<div class="p-2 text-white bg-danger">';

			foreach ($nlError as $_e) {
				if (trim($_e) != "") {
					echo trim($_e).'<br>';
				}
			}
			echo '</div>';
		}
		if (isset($_SESSION['nlError']) && (is_array($_SESSION['nlError'])) && (count($_SESSION['nlError']) > 0)) {
			echo '<div class="p-2 text-white bg-danger">';
			foreach ($_SESSION['nlError'] as $_e) {
				if (trim($_e) != "") {
					echo trim($_e).'<br>';
				}
			}
			echo '</div>';
		}

		if ($formSubmitOk == true) {
			echo '<div class="p-2 text-white bg-success">';
			echo 'Vaša prijava je zaprimljena, zahvaljujemo na interesu';
			echo '</div>';

		}

		?>
		<form action="" method="post" name="newsletter_registration_form" class="pt-2 pb-2 ampeu_form " id="newsletter_registration_form" >

			<div class="form-group">
				<label for="ime">Ime:</label>
				<input type="text" class="form-control" name="ime" id="ime" placeholder="unesite ime"  value="<?php if (isset($_POST['ime'])) { echo $_POST['ime']; } ?>">
			</div>

			<div class="form-group">
				<label for="prezime">Prezime:</label>
				<input type="text" class="form-control" name="prezime" id="prezime" placeholder="unesite prezime"  value="<?php if (isset($_POST['prezime'])) { echo $_POST['prezime']; } ?>">
			</div>

			<div class="form-group">
				<label for="email">E-mail adresa: <span class="text-danger">*</span></label>
				<input type="email" class="form-control" name="email" id="email" placeholder="korisnik@domena.com" required value="<?php if (isset($_POST['email'])) { echo $_POST['email']; } ?>">
			</div>

			<div class="form-group">
				<label for="organizacija_vrsta">Vrsta organizacije/ustanove: <span class="text-danger">*</span></label>
				<input type="text" class="form-control" name="organizacija_vrsta" id="organizacija_vrsta" placeholder="unesite vrstu organizacije"  value="<?php if (isset($_POST['organizacija_vrsta'])) { echo $_POST['organizacija_vrsta']; } ?>" required>

			</div>

			<div class="form-group">
				<label for="organizacija_naziv">Naziv organizacije/ustanove: <span class="text-danger">*</span></label>
				<input type="text" class="form-control" name="organizacija_naziv" id="organizacija_naziv" placeholder="unesite naziv organizacije/ustanove" required value="<?php if (isset($_POST['organizacija_naziv'])) { echo $_POST['organizacija_naziv']; } ?>">
			</div>

			<div class="form-group">
				<label for="organizacija_zupanija">Županija organizacije/ustanove:</label>
				<select class="form-control select2" name="organizacija_zupanija" id="organizacija_zupanija" data-placeholder="odaberite županiju organizacije/ustanove" style="width: 100%">
					<option></option>
					<?php
					if (count($counties) > 0) {
						foreach ($counties as $_county) {
							$selected = '';
							if (isset($_POST['organizacija_zupanija']) && ($_POST['organizacija_zupanija'] == $_county['id'])) {
								$selected = ' selected';
							}
							?>
							<option value="<?php echo $_county['id']; ?>" <?php echo $selected; ?>><?php echo $_county['name'];?></option>
							<?php
						} // END foreach ($counties as $_county) {
					} // END if (count($counties) > 0) {

					?>
				</select>
			</div>

			<?php
			// 2020-06-08 autocomplete removed
			?>

			<div class="form-group">
				<label for="organizacija_mjesto">Mjesto organizacije/ustanove:</label>

				<input type="text" class="form-control" name="organizacija_mjesto" id="organizacija_mjesto" placeholder="mjesto/grad ustanove"  value="<?php if (isset($_POST['organizacija_mjesto'])) { echo $_POST['organizacija_mjesto']; } ?>">
			</div>

			<div class="form-group">
				<label for="organizacija_adresa">Adresa organizacije/ustanove:</label>
				<input type="text" class="form-control" name="organizacija_adresa" id="organizacija_adresa" placeholder="ulica i kućni broj ustanove"  value="<?php if (isset($_POST['organizacija_adresa'])) { echo $_POST['organizacija_adresa']; } ?>">
			</div>

			<div class="form-group">
				<label for="funkcija">Vaša funkcija u organizaciji/ustanovi:</label>
				<input type="text" class="form-control" name="funkcija" id="funkcija" placeholder="funkcija koju obnašate u ustanovi"  value="<?php if (isset($_POST['funkcija'])) { echo $_POST['funkcija']; } ?>">
			</div>


			<div class="form-group mandatoryCheckBoxGroup">
				<label for="podrucje_interesa[]">Područje interesa: <span class="text-danger">*</span></label>
					<?php
					if (count($interests) > 0) {
						foreach ($interests as $_interes) {
							$checked = ' ';
							if (isset($_POST['podrucje_interesa']) && (is_array($_POST['podrucje_interesa'])) && (in_array($_interes['id'], $_POST['podrucje_interesa']))) {
								$checked = ' checked';
							}
							?>
							<div class="form-check">
							  <input class="form-check-input podrucje_interesa_cb" name="podrucje_interesa[]" type="checkbox" value="<?php echo $_interes['id']; ?>" id="interes<?php echo $_interes['id']; ?>" <?php echo $checked; ?>>
							  <label class="form-check-label" for="interes<?php echo $_interes['id']; ?>"><?php echo $_interes['name'];?></label>
							</div>
							<?php
						} // END foreach ($interests as $_interes) {
					} // END if (count($interests) > 0) {

					?>
			</div>

			<?php
			// euroguidanceProfileId
			if (count($euroguidanceProfile) > 0) {
				?>
				<div class="form-group euroguidance">
				<label for="euroguidance_profil">Profil za Euroguidance: <span class="text-danger">*</span></label>
				<select class="form-control select2" name="euroguidance_profil" id="euroguidance_profil" data-placeholder="odaberite profil" style="width: 100%">
					<option></option>
					<?php
					if (count($euroguidanceProfile) > 0) {
						foreach ($euroguidanceProfile as $_profile) {
							$selected = '';
							if (isset($_POST['euroguidance_profil']) && ($_POST['euroguidance_profil'] == $_profile['id'])) {
								$selected = ' selected';
							}
							?>
							<option value="<?php echo $_profile['id']; ?>" <?php echo $selected; ?>><?php echo $_profile['name'];?></option>
							<?php
						} // END foreach ($euroguidanceProfile as $_profile) {
					} // END if (count($euroguidanceProfile) > 0) {

					?>

				</select>
				</div>
				<?php

			} // END if (count($euroguidanceProfile) > 0) {
			?>

			<?php
			// professionalDevelopmentActivityId
			if (count($professionalDevelopmentActivity) > 0) {
				?>
				<div class="form-group euroguidance">
				<label for="profesionalno_usavrsavanje">Područje cjeloživotnog profesionalnog usmjeravanja (CPU) koje vas zanima: <span class="text-danger">*</span></label>
				<select class="form-control select2" name="profesionalno_usavrsavanje" id="profesionalno_usavrsavanje" data-placeholder="odaberite aktivnost" style="width: 100%">
					<option></option>
					<?php
					if (count($professionalDevelopmentActivity) > 0) {
						foreach ($professionalDevelopmentActivity as $_activity) {
							$selected = '';
							if (isset($_POST['profesionalno_usavrsavanje']) && ($_POST['profesionalno_usavrsavanje'] == $_activity['id'])) {
								$selected = ' selected';
							}
							?>
							<option value="<?php echo $_activity['id']; ?>" <?php echo $selected; ?>><?php echo $_activity['name'];?></option>
							<?php
						} // END foreach ($professionalDevelopmentActivity as $_activity) {
					} // END if (count($professionalDevelopmentActivity) > 0) {

					?>

				</select>
				</div>
				<?php

			} // END if (count($professionalDevelopmentActivity) > 0) {
			?>





			<div class="form-check form-group">
			  <input class="form-check-input" type="checkbox" value="1" id="accept_terms" required>
			  <label class="form-check-label" for="accept_terms">Slažem se <a href="https://ampeu.hr/o-nama/obrada-osobnih-podataka" target="_blank" title="uvjeti korištenja podataka">uvjetima korištenja podataka</a> <span class="text-danger">*</span></label>
			</div>

			<div class="form-group row">
				<div class="col">Polja označena sa <span class="text-danger">*</span> su obavezna</div>

			</div>

			<input type="hidden" name="nl_reg_form" value="submit" />

			<div class="form-group pt-2 pb-2">
					<input class="Button Button--primary" type="submit" name="submit" value="Registriraj se" role="button" title="Registriraj se" />
			</div>

		</form>

	</div><!-- /.newsletter_from_wrap -->
		
    <?php 
	// ====================================
	// NEWSLETTER REG FORM END                 =
	// ==================================== 
	?>
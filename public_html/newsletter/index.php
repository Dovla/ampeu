<?php
session_start();

//ini_set('display_errors', '1');
//error_reporting(E_ALL);

include('inc_newsletter_form_processing.php');

?>
<!doctype html>
<html lang="hr">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Ampeu | Agencija za mobilnost i programe EU</title><meta name="generator" content="SEOmatic">
<meta name="description" content="Pomažemo pretvoriti dobre ideje u uspješne projekte koji mijenjaju društvo na bolje.">
<meta name="referrer" content="no-referrer-when-downgrade">
<meta name="robots" content="all">
<meta content="223594766476508" property="fb:profile_id">
<meta content="hr_HR" property="og:locale">
<meta content="Ampeu" property="og:site_name">
<meta content="article" property="og:type">
<meta content="https://ampeu.hr/newsletter" property="og:url">
<meta content="Agencija za mobilnost i programe EU" property="og:title">
<meta content="Pomažemo pretvoriti dobre ideje u uspješne projekte koji mijenjaju društvo na bolje." property="og:description">
<meta content="https://ampeu.hr/photos/_1200x630_fit_center-center_82_none/ampeu-vizual-4-3.jpg?mtime=1632833196" property="og:image">
<meta content="839" property="og:image:width">
<meta content="630" property="og:image:height">
<meta content="Agencija za mobilnost i programe EU" property="og:image:alt">
<meta content="https://www.instagram.com/ampeu.hr/" property="og:see_also">
<meta content="https://www.youtube.com/user/AgencijaZaMobilnost" property="og:see_also">
<meta content="https://www.linkedin.com/company/agencija-za-mobilnost-i-programe-europske-unije" property="og:see_also">
<meta content="https://www.facebook.com/AgencijaZaMobilnostiProgrameEU" property="og:see_also">
<meta content="https://twitter.com/ampeu_hr" property="og:see_also">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@ampeu_hr">
<meta name="twitter:creator" content="@ampeu_hr">
<meta name="twitter:title" content="Agencija za mobilnost i programe EU">
<meta name="twitter:description" content="Pomažemo pretvoriti dobre ideje u uspješne projekte koji mijenjaju društvo na bolje.">
<meta name="twitter:image" content="https://ampeu.hr/photos/_800x418_fit_center-center_82_none/ampeu-vizual-4-3.jpg?mtime=1632833196">
<meta name="twitter:image:width" content="557">
<meta name="twitter:image:height" content="418">
<meta name="twitter:image:alt" content="Agencija za mobilnost i programe EU">
<link href="https://ampeu.hr/newsletter" rel="canonical">
<link href="https://ampeu.hr" rel="home">
<link type="text/plain" href="https://ampeu.hr/humans.txt" rel="author"></head>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
<link rel="stylesheet" href="https://ampeu.hr/css/main.efe46cba9bf4f3da77bc.min.css">


<style>
	.ampeu_form .form-check-input {
		margin-top: .1rem;
	}
	.ampeu_form .euroguidance {
		display: none;
	}
	.ampeu_form .etwinning {
		display: none;
	}

	.ampeuNotValid {
		/*box-shadow: 0 0 3px 1px #C70F18;*/
	}
	.ampeuNotValid label {
		color: #C70F18 !important;
	}

	.ampeuNotValid input[type="checkbox"] {
		box-shadow: 0 0 3px 1px #C70F18;
	}
	a.ampeu_btn_bootstrap, a.ampeu_btn_bootstrap:active, a.ampeu_btn_bootstrap:visited {
		border-radius: 0;
		color: rgb(0, 0, 0) !important;
		background-color: #E1E1E1;
		border: 1px solid #ADADAD;
	}
	.ampeu_btn_bootstrap {
		border-radius: 0;
		color: rgb(0, 0, 0) !important;
		background-color: #E1E1E1;
		border: 1px solid #ADADAD;
		cursor: pointer;
		padding-top: .5rem;
	}
	a.ampeu_btn_bootstrap:hover, .ampeu_btn_bootstrap:hover {
		text-decoration: none;
		background-color: #E5F1FB;
		border: 1px solid #0078D7;	
	}

	.ampeu_btn_bootstrap.disabled {
		border-radius: 0;
		color: rgb(0, 0, 0) !important;
		background-color: #E1E1E1;
		border: 1px solid #ADADAD;
		cursor: default;
	}
	a.ampeu_btn_bootstrap.disabled:hover, .ampeu_btn_bootstrap.disabled:hover {
		text-decoration: none;
		background-color: #E1E1E1;
		border: 1px solid #ADADAD;
	}
</style>
</head>

<body>
	<main>
		<section class="Section Section--bg-white">
		    <div class="Section__body">
		    	<div class="TextComponent__container">
		    	<img src="ampeu_vjesnik.jpg" alt="Ampeu vjesnik ilustracija">
				<h1>Prijavite se na Vjesnik AMPEU-a</h1>
				<div class="Text Text--content">
				<p>Ako želite redovito dobivati najave, objave i priopćenja o aktivnostima Agencije za mobilnost i programe EU molimo vas da se pribilježite u obrascu ispod.</p>
				<h2>Obrada osobnih podataka</h2>
				<p>Agencija za mobilnost i programe Europske unije obrađuje vaše osobne podatke za potrebe slanja e-obavijesti. Više o obradi vaših osobnih podataka možete pročitati u Općoj obavijesti o obradi osobnih podataka. S naše liste se možete uvijek odjaviti. Upute za odjavu navedene su u svakoj našoj e-obavijesti.</p>
				</div>
				<?php include("inc_newsletter_registration_form.php");?>
			</div>
			</div>
		</section>
	</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
<script>

$(function() {

	

	$(".podrucje_interesa_cb").on("change", function(){
		var selFieldName = $(this).parent().find('label').text();
		var segmentName = '';

		if (selFieldName == 'Euroguidance') {
			segmentName = 'euroguidance';
		} else if (selFieldName == 'eTwinning') {
			//segmentName = 'etwinning';
		}
		if ($(this).prop('checked')) {
			onOff = 1;
		} else {
			onOff = 0;
		}
		// Euroguidance
		// eTwinning
	   toggleSegment (segmentName, onOff);
	});

	function toggleSegment (segmentName, onOff) {
		if (segmentName) {
			var segmentObj = $("."+segmentName);
			if (segmentObj) {
				if (onOff == '1') {
					segmentObj.fadeIn();
					segmentObj.find('select').attr('required', 'required');
					segmentObj.find('select').removeAttr('disabled');
					segmentObj.find('input[type="text"]').attr('required', 'required');
					segmentObj.find('input').removeAttr('disabled');
				} else if (onOff == '0') {
					segmentObj.fadeOut();
					segmentObj.find('select').removeAttr('required');
					segmentObj.find('select').attr('disabled', 'disabled');
					segmentObj.find('input[type="text"]').removeAttr('required');
					segmentObj.find('input').attr('disabled', 'disabled');
				}
			}

		}
	}

	$('input[name="podrucje_interesa[]"]').each(function() {
		var fieldName = $(this).parent().find('label').text();

		if ((fieldName == 'Euroguidance') && ($(this).prop('checked'))) {
			toggleSegment ('euroguidance', 1);
		}

		if ((fieldName == 'eTwinning') && ($(this).prop('checked'))) {
			//toggleSegment ('etwinning', 1);
		}
	});



	$("#newsletter_registration_form").on("submit", function(e){
		var formValid = true;

		$(".mandatoryCheckBoxGroup").each(function(){
			var cbGroupChecked = true;
			if ($(this).css("display") == 'block') {
				$(this).removeClass('ampeuNotValid');
				cbGroupChecked = false;
				var cbElems = $(this).find('input[type="checkbox"]');
				cbElems.each(function(){
					if (!($(this).attr('disabled')) && ($(this).prop('checked'))) {
						cbGroupChecked = true;
					}
				});
				if (cbGroupChecked == false) {
					$(this).addClass('ampeuNotValid');
				}
			}
			formValid = formValid && cbGroupChecked;
		});
		$(this).addClass('was-validated');
		
		if (formValid) {
			return true;
		} else {
			e.preventDefault();
			return false;
		}
		//return false;

	});

	$(".mandatoryCheckBoxGroup").find('input[type="checkbox"]').on('change', function(){
		$("#newsletter_registration_form").removeClass('was-validated');
		var group = $(this).parentsUntil('form');
		if (group.hasClass('ampeuNotValid')) {
			group.removeClass('ampeuNotValid');
		}
	});
	


});

</script>

</body>
</html>
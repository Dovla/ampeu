<?php
/*
* nlform
* class for cummunicating with AMPEU newsletter back end
* (c) Hajtek Studio 2020
*/
/*
GET /api/provisioning/getLocationByName/{naziv mjesta}
GET /api/provisioning/professionalDevelopmentActivity
GET /api/provisioning/euroguidanceProfiles
GET /api/provisioning/fieldOfInterest
GET /api/provisioning/counties
2021-06-25 removed - text input field
GET /api/provisioning/organizationType


*/

//ini_set('display_errors', '1');
//error_reporting(E_ALL);

class nlform {

	private static $url = 'http://193.198.64.74:5100';
	private static $auth = '2hC|Nw117F13AD9769E998pK2oB1jA06#Mj5Tp7oXcf7.I';

	private static $user_agent = 'Ampeu nl form client';


	/*
	* removed 2021-06-25
	*
	*/
	public static function organizationType () {

		$url = self::$url.'/api/provisioning/organizationType';

		return(self::doHttpRequest($url, 'GET'));

	}


	public static function counties () {

		$url = self::$url.'/api/provisioning/counties';

		return(self::doHttpRequest($url, 'GET'));

	}

	public static function fieldOfInterest () {

		$url = self::$url.'/api/provisioning/fieldOfInterest';

		return(self::doHttpRequest($url, 'GET'));

	}

	public static function euroguidanceProfiles () {

		$url = self::$url.'/api/provisioning/euroguidanceProfiles';

		return(self::doHttpRequest($url, 'GET'));

	}


	public static function professionalDevelopmentActivity () {

		$url = self::$url.'/api/provisioning/professionalDevelopmentActivity';

		return(self::doHttpRequest($url, 'GET'));

	}



	/*
	* getLocationByName
	* @input: $name [string]
	* @output: doHttpRequest result
	*
	*
	*/
	public static function getLocationByName ($name) {
		///api/provisioning/getLocationByName/{naziv mjesta}
		$url = self::$url.'/api/provisioning/getLocationByName/';

		if ($name != "") {
			$name = trim($name);

			$url = $url.$name;

		}

		return(self::doHttpRequest($url, 'GET'));

	}


	public static function doHttpRequest ($url, $method='GET', $header=array(), $postdata=array()) {

		$result = array('error'=>'', 'errordesc'=>'', 'result_code'=>'', 'header'=>array(), 'content'=>array());

		if ($url != "") {

			$ch = curl_init();

			//echo $url.'<br>';

			curl_setopt($ch, CURLOPT_URL, $url);

			if ((ini_get('open_basedir') == '') && (ini_get('safe_mode' == 'Off'))) {
				curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
			}

			$header_array = array('Authorization: '.self::$auth);

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, self::$user_agent);

			if (isset($postdata) && (count($postdata) > 0)) {

				$header_array[] = 'Content-Type: application/json';
				//curl_setopt($ch, CURLOPT_POST, true);
				$curl_post_data = json_encode($postdata);
				//curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(($postdata)));
				curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_post_data);
				//
				//multipart/form-data
				// application/x-www-form-urlencoded
				//$header_array[] = 'Accept: application/json';

				//$header_array[] = 'Content-Type: multipart/form-data';

			}

			// return headers?
			curl_setopt($ch, CURLOPT_HEADER, true);

			// set specific headers
			//$header_array = array('Accept: application/json', 'Authorization: Basic '.self::$auth);


			curl_setopt($ch, CURLOPT_HTTPHEADER, $header_array);

			//curl_setopt($ch, CURLOPT_ENCODING, "gzip");

			$data = curl_exec($ch);

			$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			$curl_errno = curl_errno($ch);
	        $curl_error = curl_error($ch);

			curl_close($ch);

			/*
			CURL OK
			curl code: 200
			curl error:
			curl errorno: 0

			*/
			/*
			echo "curl code: ".$code."<br>";
			echo "curl error: ".$curl_error."<br>";
			echo "curl errorno: ".$curl_errno."<br>";
			echo "data len:".strlen($data)."<br>";
			print_r($data);
			echo '<br>';
			*/
			$header = null;

			// separate header by \r\n\r\n
			if (strpos($data, "\r\n\r\n") !== false) {
				$result_arr = explode("\r\n\r\n", $data);
				/*
				0 is header
				1 is actual data
				*/
				$header = explode("\r\n", $result_arr[0]);
				$data = $result_arr[1];
				/*
				echo "header: <br>";
				print_r($header);
				echo "<br>";
				*/
				foreach ($header as $header_item) {

					//echo "header item: ".$header_item."<br>";

					if (strpos($header_item, 'Content-Type: application/json') !== false) {
						// we received json, decode it
						$data = json_decode($data, true);
					}

				}
			}



			if ($curl_errno == 200) {
				// operation ok
				$curl_errno = null;

			}

			if ($code != 200) {
				// some error happened
			}

			/*
			$data:
			 [status] => SUCCESS
			 [message] =>
			 [value] => Array (
			 	[0] => Array ( [id] => 1616 [name] => ZAGRAD [postCode] => 23223 [status] => 1 )
				[1] => Array ( [id] => 4491 [name] => ZAGRADCI [postCode] => 47250 [status] => 1 )
				[2] => Array ( [id] => 2497 [name] => ZAGRAĐE [postCode] => 34310 [status] => 1 )
				[3] => Array ( [id] => 4130 [name] => ZAGRAJ [postCode] => 47000 [status] => 1 )
				[4] => Array ( [id] => 1 [name] => ZAGREB [postCode] => 10000 [status] => 1 ) )
			 [requestId] => 3b9d4b904c7f452bbcd864cd10d1b1d8
			 )

			*/

			$result['error'] = $curl_errno;
			$result['errordesc'] = $curl_error;
			$result['result_code'] = $code;
			$result['content'] = $data;
			$result['header'] = $header;


		} else {
			$result['error'] = 999;
			$result['errordesc'] = 'url not specified';
		}

		return $result;

	}

	public static function filterInput ($string) {
		$rez = "";
		$filter = array("select ", " and ", " or ", "<", "&gt;", ">", "&lt;", "script", "function", "http", "ftp", "*", "  ", "src", ";", "%", ".js", "#", "alert", "\\", "0d", "0a", "3b", "\"", "%3D", "=", "drop", "insert");
		$rez = str_replace($filter, " ", $string);
		$rez = trim($rez);
		return $rez;
	}

	public static function removeDoubleQuotes ($string) {
		if (strpos($string, '"') !== false) {
			$string = str_replace('"', "`", $string);
		}
		return $string;
	}
	
	public static function stripWordHtml ($text, $allowed_tags = '') {
		if (strpos($text, "&nbsp;") !== false) {
			$text = str_replace("&nbsp;", " ", $text);
		}
		if (strpos($text, "&#8209;") !== false) {
			$text = str_replace("&#8209;", "-", $text);
		}
		mb_regex_encoding('UTF-8');
		//replace MS special characters first
		$search = array('/&lsquo;/u', '/&rsquo;/u', '/&ldquo;/u', '/&rdquo;/u', '/&mdash;/u');
		$replace = array('\'', '\'', '"', '"', '-');
		$text = preg_replace($search, $replace, $text);
		//make sure _all_ html entities are converted to the plain ascii equivalents - it appears
		//in some MS headers, some html entities are encoded and some aren't
		$text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
		//try to strip out any C style comments first, since these, embedded in html comments, seem to
		//prevent strip_tags from removing html comments (MS Word introduced combination)
		if(mb_stripos($text, '/*') !== FALSE){
			$text = mb_eregi_replace('#/\*.*?\*/#s', '', $text, 'm');
		}
		//introduce a space into any arithmetic expressions that could be caught by strip_tags so that they won't be
		//'<1' becomes '< 1'(note: somewhat application specific)
		$text = preg_replace(array('/<([0-9]+)/'), array('< $1'), $text);
		$text = strip_tags($text, $allowed_tags);
		//eliminate extraneous whitespace from start and end of line, or anywhere there are two or more spaces, convert it to one
		$text = preg_replace(array('/^\s\s+/', '/\s\s+$/', '/\s\s+/u'), array('', '', ' '), $text);
		//strip out inline css and simplify style tags
		$search = array('#<(strong|b)[^>]*>(.*?)</(strong|b)>#isu', '#<(em|i)[^>]*>(.*?)</(em|i)>#isu', '#<u[^>]*>(.*?)</u>#isu');
		$replace = array('<b>$2</b>', '<i>$2</i>', '<u>$1</u>');
		$text = preg_replace($search, $replace, $text);
		//on some of the ?newer MS Word exports, where you get conditionals of the form 'if gte mso 9', etc., it appears
		//that whatever is in one of the html comments prevents strip_tags from eradicating the html comment that contains
		//some MS Style Definitions - this last bit gets rid of any leftover comments */
		$num_matches = preg_match_all("/\<!--/u", $text, $matches);
		if($num_matches){
			  $text = preg_replace('/\<!--(.)*--\>/isu', '', $text);
		}
		return $text;
	}
	
	public static function checkEmail($mail) {
		$rez = false;
		// provjeri mail
		$local = '[a-z0-9_\+-]';
		$alnum = 'a-z0-9';
		$alnum_tld = 'a-z0-9\.';
		$domain = "([$alnum]([-$alnum]*[$alnum]+)?)";

		$arr  = array();
		$arr['start'] = '/^';
		$arr['local'] = "$local+(\.$local+)*";
		$arr['at'] = '@';
		$arr['domain'] = "($domain{1,63}\.)+";
		$arr['tld'] = "[$alnum_tld]{2,6}";
		$arr['end'] = '$/i';

		$regex  = implode('',$arr);

		if (preg_match($regex, $mail)) {
			$rez = true;
		}

		return $rez;

	}

} // end class



?>